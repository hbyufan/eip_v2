﻿using EIP.Common.Models.Attributes.MicroOrm;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EIP.Base.Models.Entities.WeChat
{
    /// <summary>
    /// 微信帐号信息
    /// </summary>
    [Serializable]
    [Table("wechat_account")]
    public class WeChatAccount
    {
        /// <summary>
        /// 自增Id
        /// </summary>
        [Key, Identity, JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// 主键Id
        /// </summary>
        public Guid WeChatAccountId { get; set; }

        /// <summary>
        /// 模版代码
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 类型:1公众号,2小程序,3企业微信
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// AppId
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

    }
}
