using EIP.Common.Models.Attributes.MicroOrm;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EIP.Base.Models.Entities.System
{
    /// <summary>
    /// 移动端按钮
    /// </summary>
    [Serializable]
	[Table("System_MobileMenuButton")]
    public class SystemMobileMenuButton
    {
        /// <summary>
        /// 消息Id
        /// </summary>		
        [Key, Identity, JsonIgnore]
        public long Id { get; set; }

        /// <summary>
        /// 菜单按钮id
        /// </summary>		
        public Guid MobileMenuButtonId{ get; set; }
       
        /// <summary>
        /// 菜单id
        /// </summary>		
		public Guid MobileMenuId{ get; set; }
       
        /// <summary>
        /// 
        /// </summary>		
		public string MobileMenuName{ get; set; }
       
        /// <summary>
        /// 名称
        /// </summary>		
		public string Name{ get; set; }
       
        /// <summary>
        /// 图标
        /// </summary>		
		public string Icon{ get; set; }

        /// <summary>
        /// 执行方法
        /// </summary>		
		public string Method{ get; set; }
       
        /// <summary>
        /// 自定义Script
        /// </summary>		
		public string Script{ get; set; }
       
        /// <summary>
        /// 排序号
        /// </summary>		
		public int? OrderNo{ get; set; }
       
        /// <summary>
        /// 备注
        /// </summary>		
		public string Remark{ get; set; }
       
        /// <summary>
        /// 是否冻结
        /// </summary>		
		public bool? IsFreeze{ get; set; }

        /// <summary>
        /// 0Api,1方法
        /// </summary>
        public short Type { get; set; }

        /// <summary>
        /// Api地址
        /// </summary>
        public string ApiPath { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 创建用户Id
        /// </summary>		
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 创建人员名称
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 创建用户Id
        /// </summary>		
        public Guid? UpdateUserId { get; set; }

        /// <summary>
        /// 修改人员名称
        /// </summary>
        public string UpdateUserName { get; set; }
    } 
}
