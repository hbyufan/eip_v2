﻿using System;

namespace EIP.Workflow.Models.Dtos.Process
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkflowApproveUserSqlOutput
    {
        /// <summary>
        /// 数据库ID
        /// </summary>
        public Guid DataBaseId { get; set; }
        /// <summary>
        /// 规则SQL
        /// </summary>
        public string RuleSql { get; set; }
    }
}
