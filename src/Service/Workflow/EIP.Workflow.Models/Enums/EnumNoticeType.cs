﻿/**************************************************************
* Copyright (C) 2018 www.sf-info.cn 盛峰版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
namespace EIP.Workflow.Models.Enums
{
    /// <summary>
    /// 通知类型枚举
    /// </summary>
    public enum EnumNoticeType
    {
        /// <summary>
        /// 
        /// </summary>
        邮件 = 0,
        /// <summary>
        /// 
        /// </summary>
        站内 = 1,
        /// <summary>
        /// 
        /// </summary>
        短信 = 2,
        /// <summary>
        /// 
        /// </summary>
        微信公众号 = 3,
        /// <summary>
        /// 
        /// </summary>
        微信小程序 = 4,
        /// <summary>
        /// 企业微信
        /// </summary>
        企业微信 = 5,
        /// <summary>
        /// QQ
        /// </summary>
        Qq = 6,
        /// <summary>
        /// 钉钉
        /// </summary>
        钉钉 = 7,
        /// <summary>
        /// App
        /// </summary>
        App = 8,
    }

    /// <summary>
    /// 通知触发类型
    /// </summary>
    public enum EnumNoticeTrigger
    {
        /// <summary>
        /// 
        /// </summary>
        下一步成功 = 0,
        /// <summary>
        /// 
        /// </summary>
        退回成功 = 1,
        /// <summary>
        /// 
        /// </summary>
        拒绝成功 = 2,
    }
}