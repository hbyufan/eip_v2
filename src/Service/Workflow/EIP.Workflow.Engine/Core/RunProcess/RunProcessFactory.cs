﻿/**************************************************************
* Copyright (C) 2018 www.sf-info.cn 盛峰版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Models;
using EIP.Workflow.Engine.Models.Dtos.ProcessInstance;
using EIP.Workflow.Engine.Models.Resx;
using System.Threading.Tasks;
using EIP.Workflow.Models.Enums;

namespace EIP.Workflow.Engine.Core.RunProcess
{
    /// <summary>
    /// 工厂
    /// </summary>
    internal static class RunProcessFactory
    {
        /// <summary>
        /// 创建走向流程
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        internal static OperateStatus CreateRunProcess(WorkflowEngineRunProcessInput input)
        {
            OperateStatus operate = new OperateStatus();
            try
            {
                BaseRunProcess runProcess;
                switch (input.Type)
                {
                    case EnumAcitvityType.开始:
                        runProcess = new StartRunProcess();
                        break;
                    case EnumAcitvityType.审批:
                        //runProcess = new TaskRunProcess();
                        break;
                    case EnumAcitvityType.子流程:
                        //runProcess = new SubprocessRunProcess();
                        break;
                    case EnumAcitvityType.结束:
                        runProcess = new EndRunProcess();
                        break;
                    default:
                        operate.Msg = ResourceWorkflowEngine.未匹配活动类型;
                        return operate;
                }
                //runProcess.runProcessInput = input;
                //return runProcess.RunProcess();
            }
            catch (WorkflowEngineException e)
            {
                operate.Msg = e.Message;
            }
            return operate;
        }
    }
}
