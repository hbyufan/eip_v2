﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Entities.Workflow;
using EIP.Base.Repository.Fixture;
using EIP.Common.Core.Context;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Paging;
using EIP.Common.Models.Tree;
using EIP.Common.Util;
using EIP.Workflow.Models.Dtos;
using EIP.Workflow.Models.Dtos.Process;
using EIP.Workflow.Repository.IRepository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EIP.Workflow.Logic.Imp
{
    /// <summary>
    /// 工作流处理界面按钮接口实现
    /// </summary>
    public class WorkflowProcessLogic : DapperAsyncLogic<WorkflowProcess>, IWorkflowProcessLogic
    {
        #region 构造函数

        private readonly IWorkflowProcessRepository _processRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="processRepository"></param>
        /// <param name="systemDictionaryRepository"></param>
        public WorkflowProcessLogic(IWorkflowProcessRepository processRepository)
        {
            _processRepository = processRepository;
        }

        #endregion

        #region 方法

        /// <summary>
        /// 根据流程类型获取所有流程
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<PagedResults<WorkflowProcessFindOutput>>> Find(WorkflowProcessFindInput input)
        {
            return OperateStatus<PagedResults<WorkflowProcessFindOutput>>.Success(await _processRepository.Find(input));
        }

        /// <summary>
        /// 获取历史版本
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<WorkflowProcess>>> FindVersion(IdInput input)
        {
            return OperateStatus<IEnumerable<WorkflowProcess>>.Success(await FindAllAsync(f => f.ProcessParentId == input.Id));
        }

        /// <summary>
        /// 删除表单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus> Delete(IdInput<string> input)
        {
            OperateStatus operateStatus = new OperateStatus();
            foreach (var item in input.Id.Split(","))
            {
                var processId = Guid.Parse(item);
                operateStatus = await UpdateAsync(u => u.ProcessId == processId, new { IsDelete = true });
            }
            return operateStatus;
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public async Task<OperateStatus> Save(WorkflowProcess process)
        {
            var workflowProcess = await FindAsync(f => f.ProcessId == process.ProcessId);
            var currentUser = EipHttpContext.CurrentUser();
            if (workflowProcess == null)
            {
                process.Version = 1.0;
                process.CreateTime = DateTime.Now;
                process.CreateUserId = currentUser.UserId;
                process.CreateUserName = currentUser.Name;
                process.UpdateTime = DateTime.Now;
                process.UpdateUserId = currentUser.UserId;
                process.UpdateUserName = currentUser.Name;

                process.SaveJson = process.SaveJson.Replace("{0}", process.Name);
                process.SaveJson = process.SaveJson.Replace("{1}", CombUtil.NewComb().ToString());
                process.SaveJson = process.SaveJson.Replace("{2}", CombUtil.NewComb().ToString());
                return await InsertAsync(process);
            }
            process.Id = workflowProcess.Id;
            process.Version = workflowProcess.Version;
            process.CreateTime = workflowProcess.CreateTime;
            process.CreateUserId = workflowProcess.CreateUserId;
            process.CreateUserName = workflowProcess.CreateUserName;
            process.UpdateTime = DateTime.Now;
            process.UpdateUserId = process.CreateUserId;
            process.UpdateUserName = process.CreateUserName;
            process.PublicJson = workflowProcess.PublicJson;
            process.SaveJson = workflowProcess.SaveJson;
            return await UpdateAsync(process);
        }

        /// <summary>
        /// 复制
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public async Task<OperateStatus> Copy(WorkflowProcess process)
        {
            using (var fixture = new SqlDatabaseFixture(false))
            {
                var workflowProcess = await FindByIdAsync(process.ProcessId);
                process.CreateTime = DateTime.Now;
                process.ProcessId = CombUtil.NewComb();
                var activitys =
                    (await fixture.Db.WorkflowProcessActivity.FindAllAsync(f => f.ProcessId == process.ProcessId))
                    .ToList();
                var links = (await fixture.Db.WorkflowProcessLink.FindAllAsync(f => f.ProcessId == process.ProcessId))
                    .ToList();

                foreach (var activity in activitys)
                {
                    activity.ProcessId = process.ProcessId;
                }

                foreach (var line in links)
                {
                    line.ProcessId = process.ProcessId;
                }

                if (activitys.Any())
                    await fixture.Db.WorkflowProcessActivity.BulkInsertAsync(activitys);
                if (links.Any())
                    await fixture.Db.WorkflowProcessLink.BulkInsertAsync(links);
                process.Version = 1.0;
                process.SaveJson = workflowProcess.SaveJson;
                process.PublicJson = workflowProcess.PublicJson;
                return await InsertAsync(process);
            }
        }

        /// <summary>
        /// 保存流程设计图
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public async Task<OperateStatus> SaveDesignJson(WorkflowProcessSaveInput process)
        {
            return process.IsNewVersion ? await SaveNewDesignJson(process) : await SaveUpdateNewDesignJson(process);
        }

        /// <summary>
        /// 更新流程设计图
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        private async Task<OperateStatus> SaveUpdateNewDesignJson(WorkflowProcessSaveInput process)
        {
            using (var fixture = new SqlDatabaseFixture(false))
            {
                var operateStatus = new OperateStatus();
                var workflowProcess = await FindAsync(f => f.ProcessId == process.ProcessId);
                try
                {
                    bool insert = workflowProcess == null;
                    if (insert)
                    {
                        workflowProcess = new WorkflowProcess();
                        workflowProcess.ProcessId = process.ProcessId;
                        workflowProcess.Name = process.Name;
                        workflowProcess.Icon = process.Icon;
                        workflowProcess.CreateTime = DateTime.Now;
                        workflowProcess.CreateUserId = process.UpdateUserId;
                        workflowProcess.CreateUserName = process.UpdateUserName;
                        workflowProcess.Theme = process.Theme;
                        workflowProcess.Version = process.Version;
                        workflowProcess.ShowLibrary = process.ShowLibrary;
                        workflowProcess.FormId = process.FormId;
                        workflowProcess.Sn = process.Sn;
                        workflowProcess.TypeId = process.TypeId;
                        workflowProcess.SaveJson = process.DesignJson;
                        if (process.IsPublic)
                        {
                            workflowProcess.PublicJson = process.DesignJson;
                        }
                    }
                    workflowProcess.SaveJson = process.DesignJson;
                    if (process.IsPublic)
                    {
                        workflowProcess.PublicJson = process.DesignJson;
                    }
                    workflowProcess.UpdateUserId = process.UpdateUserId;
                    workflowProcess.UpdateUserName = process.UpdateUserName;
                    workflowProcess.UpdateTime = process.UpdateTime;
                    await fixture.Db.WorkflowProcessActivity.DeleteAsync(d => d.ProcessId == process.ProcessId);
                    await fixture.Db.WorkflowProcessLink.DeleteAsync(d => d.ProcessId == process.ProcessId);

                    if (process.Activities.Any())
                    {
                        IList<WorkflowProcessActivity> activities = new List<WorkflowProcessActivity>();
                        foreach (var activity in process.Activities)
                        {
                            activities.Add(new WorkflowProcessActivity
                            {
                                ActivityId = activity.ActivityId,
                                ProcessId = process.ProcessId,
                                FormId = activity.Base.FormId,
                                Type = activity.Type,
                                Title = activity.Title,
                                Json = activity.Json,
                            });
                        }

                        await fixture.Db.WorkflowProcessActivity.BulkInsertAsync(activities);
                    }

                    if (process.Links != null && process.Links.Any())
                    {
                        foreach (var link in process.Links)
                        {
                            link.ProcessId = process.ProcessId;
                        }
                        await fixture.Db.WorkflowProcessLink.BulkInsertAsync(process.Links);
                    }

                    operateStatus = insert ? await InsertAsync(workflowProcess) : await UpdateAsync(workflowProcess);
                }
                catch (Exception exception)
                {
                    operateStatus.Msg = exception.Message;
                }

                return operateStatus;
            }
        }

        /// <summary>
        /// 保存新流程设计图
        ///     1、新建一条流程老版本记录
        ///     2、更新老流程版本号及流程设计图
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        private async Task<OperateStatus> SaveNewDesignJson(WorkflowProcessSaveInput process)
        {
            using (var fixture = new SqlDatabaseFixture(false))
            {
                var operateStatus = new OperateStatus();
                var workflowProcess = await FindAsync(f => f.ProcessId == process.ProcessId);
                try
                {
                    bool insert = workflowProcess == null;
                    if (insert)
                    {
                        workflowProcess = new WorkflowProcess();
                        workflowProcess.ProcessId = process.ProcessId;
                        workflowProcess.Name = process.Name;
                        workflowProcess.Icon = process.Icon;
                        workflowProcess.CreateTime = DateTime.Now;
                        workflowProcess.CreateUserId = process.UpdateUserId;
                        workflowProcess.CreateUserName = process.UpdateUserName;
                        workflowProcess.Theme = process.Theme;
                        workflowProcess.Version = process.Version;
                        workflowProcess.ShowLibrary = process.ShowLibrary;
                        workflowProcess.FormId = process.FormId;
                        workflowProcess.Sn = process.Sn;
                        workflowProcess.SaveJson = process.DesignJson;
                        workflowProcess.TypeId = process.TypeId;
                        if (process.IsPublic)
                        {
                            workflowProcess.PublicJson = process.DesignJson;
                        }
                    }
                    #region 处理老版本

                    workflowProcess.ProcessId = CombUtil.NewComb();
                    workflowProcess.ProcessParentId = process.ProcessId;
                    //获取活动和连线
                    var allActivitys = (await fixture.Db.WorkflowProcessActivity.FindAllAsync(f => f.ProcessId == process.ProcessId)).ToList();
                    var allLinks = (await fixture.Db.WorkflowProcessLink.FindAllAsync(f => f.ProcessId == process.ProcessId)).ToList();
                    if (allActivitys.Any())
                    {
                        foreach (var item in allActivitys)
                        {
                            item.ActivityId = CombUtil.NewComb();
                            item.ProcessId = workflowProcess.ProcessId;
                        }

                        await fixture.Db.WorkflowProcessActivity.BulkInsertAsync(allActivitys);
                    }

                    if (allLinks.Any())
                    {
                        foreach (var item in allLinks)
                        {
                            item.LinkId = CombUtil.NewComb();
                            item.ProcessId = workflowProcess.ProcessId;
                        }

                        await fixture.Db.WorkflowProcessLink.BulkInsertAsync(allLinks);
                    }

                    await InsertAsync(workflowProcess);

                    #endregion

                    //更新
                    workflowProcess.SaveJson = process.DesignJson;
                    if (process.IsPublic)
                    {
                        workflowProcess.PublicJson = process.DesignJson;
                    }
                    workflowProcess.ProcessId = process.ProcessId;
                    workflowProcess.ProcessParentId = null;
                    workflowProcess.UpdateUserId = process.UpdateUserId;
                    workflowProcess.UpdateUserName = process.UpdateUserName;
                    workflowProcess.UpdateTime = process.UpdateTime;
                    workflowProcess.Version = workflowProcess.Version + 0.1;
                    await fixture.Db.WorkflowProcessActivity.DeleteAsync(d => d.ProcessId == process.ProcessId);
                    await fixture.Db.WorkflowProcessLink.DeleteAsync(d => d.ProcessId == process.ProcessId);

                    if (process.Activities.Any())
                    {
                        IList<WorkflowProcessActivity> activities = new List<WorkflowProcessActivity>();
                        foreach (var activity in process.Activities)
                        {
                            activities.Add(new WorkflowProcessActivity()
                            {
                                ActivityId = activity.ActivityId,
                                ProcessId = process.ProcessId,
                                Type = activity.Type,
                                FormId = activity.Base.FormId,
                                Title = activity.Title,
                                Json = activity.Json,
                            });
                        }
                        await fixture.Db.WorkflowProcessActivity.BulkInsertAsync(activities);
                    }

                    if (process.Links != null && process.Links.Any())
                    {
                        await fixture.Db.WorkflowProcessLink.BulkInsertAsync(process.Links);
                    }

                    operateStatus = insert ? await InsertAsync(workflowProcess) : await UpdateAsync(workflowProcess);
                }
                catch (Exception exception)
                {
                    operateStatus.Msg = exception.Message;
                }

                return operateStatus;
            }
        }

        /// <summary>
        /// 返回所有
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IList<BaseTree>>> FindAll(IdInput input)
        {
            using (var fixture = new SqlDatabaseFixture())
            {
                var types = await fixture.Db.SystemType.FindAllAsync();
                IList<BaseTree> entities = new List<BaseTree>();
                string parentIds = input.Id + ",";
                var allProcess = (await _processRepository.FindAllProcess()).ToList();
                foreach (var type in types)
                {
                    entities.Add(new BaseTree
                    {
                        id = type.TypeId,
                        text = type.Name,
                        disableCheckbox = true
                    });
                    var process = allProcess.Where(w => w.TypeId == type.TypeId);
                    foreach (var pro in process)
                    {
                        entities.Add(new BaseTree
                        {
                            parent = type.TypeId,
                            id = pro.ProcessId,
                            text = pro.Name,
                            icon = pro.Icon
                        });
                    }
                }

                return OperateStatus<IList<BaseTree>>.Success(entities);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<WorkflowProcess>> FindById(IdInput input)
        {
            return OperateStatus<WorkflowProcess>.Success(await FindAsync(f => f.ProcessId == input.Id));
        }
        #endregion
    }
}