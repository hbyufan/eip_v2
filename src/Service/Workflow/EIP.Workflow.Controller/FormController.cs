﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Controller.Attribute;
using EIP.Common.Models.Dtos;
using EIP.Workflow.Engine.Logic;
using EIP.Workflow.Engine.Models.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EIP.Workflow.Controller
{
    /// <summary>
    /// 工作流表单控制器
    /// </summary>
    public class FormController : BaseWorkflowController
    {
        #region 构造函数        
        private readonly IWorkflowEngineFormLogic _workflowEngineFormLogic;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workflowEngineFormLogic"></param>       
        public FormController(
            IWorkflowEngineFormLogic workflowEngineFormLogic)
        {
            _workflowEngineFormLogic = workflowEngineFormLogic;
        }
        #endregion


        #region 方法

        #region 表单
        /// <summary>
        ///根据实例Id获取列值
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("流程维护-方法-根据实例Id获取列值", RemarkFrom.Workflow)]
        [Route("/workflow/engine/column/{id}")]
        public async Task<JsonResult> FindDataBaseColumnsByProcessId([FromRoute] IdInput input)
        {
            return Json(await _workflowEngineFormLogic.FindDataBaseColumnsByProcessId(input));
        }
        #endregion
        #region 事件
        /// <summary>
        /// 根据Api处理事件
        /// </summary>
        /// <param name="input">Id</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("事件处理-方法-根据Api处理事件", RemarkFrom.System)]
        public async Task<object> EventDoByApi(WorkflowEngineEventDoByApiInput input)
        {
            input.Header = "Authorization:" + Request.Headers["Authorization"];
            return await _workflowEngineFormLogic.EventDoByApi(input);
        }
        #endregion
        #endregion
    }
}