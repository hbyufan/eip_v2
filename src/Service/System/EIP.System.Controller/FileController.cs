/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Controller.Attribute;
using EIP.Common.Language.Resource;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Util;
using EIP.Common.Extension;
using EIP.System.Logic;
using EIP.Base.Models.Entities.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using SystemIo = System.IO;
using System.Text;
using System.Web;
using EIP.Common.Log;

namespace EIP.System.Controller
{
    /// <summary>
    /// 文件存储表
    /// </summary>
    public class FileController : BaseSystemController
    {
        #region 构造函数
        private readonly ISystemFileLogic _systemFileLogic;

        /// <summary>
        /// 文件存储表构造函数
        /// </summary>
        /// <param name="systemFileLogic"></param>
        public FileController(ISystemFileLogic systemFileLogic)
        {
            _systemFileLogic = systemFileLogic;
        }

        #endregion

        #region 方法
        /// <summary>
        /// 一次性获取
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Description("文件存储表-方法-列表-一次性获取")]
        [Route("/system/file/query")]
        public async Task<JsonResult> FindLoadonce()
        {
            return Json(await _systemFileLogic.FindAllAsync());
        }

        /// <summary>
        /// 根据Id获取
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Description("文件存储表-方法-编辑-根据Id获取")]
        [Route("/system/file/{id}")]
        public async Task<JsonResult> FindById([FromRoute] IdInput input)
        {
            return Json(await _systemFileLogic.FindByIdAsync(input.Id));
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("文件存储表-方法-编辑-保存")]
        [Route("/system/file/save")]
        public async Task<JsonResult> Save(SystemFile input)
        {
            return Json(await _systemFileLogic.Save(input));
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input">主键集合</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("文件存储表-方法-列表-删除")]
        [Route("/system/file/{id}")]
        public async Task<JsonResult> Delete(IdInput<string> input)
        {
            return Json(await _systemFileLogic.DeleteByIdsAsync(input.Id));
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Route("/system/file/upload")]
        public async Task<JsonResult> UploadFile()
        {
            OperateStatus<IList<Guid>> operateStatus = new OperateStatus<IList<Guid>>();
            try
            {
                IList<Guid> fileIds = new List<Guid>();
                var files = Request.Form.Files;
                if (files.Any())
                {
                    foreach (var file in files)
                    {
                        var uploadPath = ConfigurationUtil.GetUploadPath();
                        if (!SystemIo.Directory.Exists(uploadPath + $"/{CurrentUser.UserId}"))
                        {
                            SystemIo.Directory.CreateDirectory(uploadPath + $"/{CurrentUser.UserId}");
                        }
                        string path = $"/{CurrentUser.UserId}/" + Guid.NewGuid() + SystemIo.Path.GetExtension(file.FileName);
                        string filename = uploadPath + path;
                        using (SystemIo.FileStream fs = SystemIo.File.Create(filename))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                            SystemFile systemFile = new SystemFile
                            {
                                Name = file.FileName,
                                Path = path,
                                ContentType = file.ContentType,
                                Size = file.Length / 1024,
                                CreateTime = DateTime.Now,
                                CreateUserId = CurrentUser.UserId
                            };
                            var insertResult = await _systemFileLogic.InsertAsync(systemFile);
                            if (insertResult.Code == ResultCode.Success)
                                fileIds.Add(systemFile.FileId);
                        }
                    }
                    operateStatus.Data = fileIds;
                    operateStatus.Code = ResultCode.Success;
                    operateStatus.Msg = Chs.Successful;
                }
                else
                {
                    operateStatus.Code = ResultCode.Success;
                    operateStatus.Msg = Chs.Successful;
                }
            }
            catch (Exception ex)
            {
                operateStatus.Msg = ex.Message;
            }
            return Json(operateStatus);
        }

        /// <summary>
        /// 修改上传文件:如修改头像图片等
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Route("/system/file/edit")]
        public async Task<JsonResult> EditUpdateFile(IdInput<int> input)
        {
            OperateStatus<IList<Guid>> operateStatus = new OperateStatus<IList<Guid>>();
            try
            {
                IList<Guid> fileIds = new List<Guid>();
                var files = Request.Form.Files;
                if (files.Any())
                {
                    foreach (var file in files)
                    {
                        var uploadPath = ConfigurationUtil.GetUploadPath();
                        if (!SystemIo.Directory.Exists(uploadPath + $"/{CurrentUser.UserId}"))
                        {
                            SystemIo.Directory.CreateDirectory(uploadPath + $"/{CurrentUser.UserId}");
                        }
                        string path = $"/{CurrentUser.UserId}/" + Guid.NewGuid() + SystemIo.Path.GetExtension(file.FileName);
                        string filename = uploadPath + path;
                        using (SystemIo.FileStream fs = SystemIo.File.Create(filename))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                            var systemFile = await _systemFileLogic.FindByIdAsync(input.Id);
                            systemFile.Name = file.FileName;
                            systemFile.Path = path;
                            systemFile.ContentType = file.ContentType;
                            systemFile.Size = file.Length / 1024;
                            var result = await _systemFileLogic.UpdateAsync(systemFile);
                            if (result.Code == ResultCode.Success)
                            {
                                //判断图片类型
                                switch (systemFile.Type)
                                {
                                    case 1://商品图片

                                        break;
                                    case 2://商家头像

                                        break;
                                }
                            }
                            fileIds.Add(systemFile.FileId);
                        }
                    }
                    operateStatus.Data = fileIds;
                    operateStatus.Code = ResultCode.Success;
                    operateStatus.Msg = Chs.Successful;
                }
                else
                {
                    operateStatus.Code = ResultCode.Success;
                    operateStatus.Msg = Chs.Successful;
                }
            }
            catch (Exception ex)
            {
                operateStatus.Msg = ex.Message;
            }
            return Json(operateStatus);
        }

        /// <summary>
        /// 获取文件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Route("/system/file/ids")]
        public async Task<JsonResult> FindFile([FromRoute] IdInput<string> input)
        {
            return Json(await _systemFileLogic.FindFile(input));
        }

        /// <summary>
        /// 获取文件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Route("/system/file/correlation/{id}")]
        public async Task<JsonResult> FindFileByCorrelationId([FromRoute] IdInput<string> input)
        {
            return Json(await _systemFileLogic.FindFileByCorrelationId(input));
        }


        /// <summary>
        /// 获取文件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Route("/system/file/image")]
        public async Task<JsonResult> FindImage([FromRoute] IdInput input)
        {
            OperateStatus<string> operateStatus = new OperateStatus<string>();
            var file = await _systemFileLogic.FindByIdAsync(input.Id);
            if (file != null)
            {
                var path = ConfigurationUtil.GetFileServer() + "upload" + file.Path;
                operateStatus.Data = path;
                operateStatus.Msg = Chs.Successful;
                operateStatus.Code = ResultCode.Success;
            }
            return Json(operateStatus);
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Route("/system/file/download")]
        public async Task<IActionResult> DownLoadFile([FromRoute] IdInput input)
        {
            var fileInfo = await _systemFileLogic.FindByIdAsync(input.Id);
            var uploadPath = ConfigurationUtil.GetUploadPath();
            string filename = uploadPath + fileInfo.Path;
            var stream = SystemIo.File.OpenRead(filename);
            return File(stream, fileInfo.ContentType, fileInfo.Name);
        }
        /// <summary>
        /// Ueditor上传文件
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Route("/system/file/upload/ueditor")]
        public string UploadUeditor(string action)
        {
            if (action == "config")
            {
                return "{\"imageActionName\":\"catchimage\",\"imageFieldName\":\"upfile\",\"imageMaxSize\":16777216,\"imageAllowFiles\":[\".png\",\".jpg\",\".jpeg\",\".gif\",\".bmp\",\".webp\",\".PNG\",\".JPG\",\".JPEG\",\".GIF\",\".BMP\",\".WEBP\"],\"imageCompressEnable\":true,\"imageCompressBorder\":1600,\"imageInsertAlign\":\"none\",\"imageUrlPrefix\":\"//wode.cos.ap-chengdu.myqcloud.com/\",\"imagePathFormat\":\"storage/image/{yyyy}{mm}{dd}/{time}\",\"scrawlActionName\":\"uploadscrawl\",\"scrawlFieldName\":\"upfile\",\"scrawlPathFormat\":\"storage/image/{yyyy}{mm}{dd}/{time}\",\"scrawlMaxSize\":16777216,\"scrawlUrlPrefix\":\"//wode.cos.ap-chengdu.myqcloud.com/\",\"scrawlInsertAlign\":\"none\",\"snapscreenActionName\":\"uploadimage\",\"snapscreenPathFormat\":\"storage/image/{yyyy}{mm}{dd}/{time}\",\"snapscreenUrlPrefix\":\"//wode.cos.ap-chengdu.myqcloud.com/\",\"snapscreenInsertAlign\":\"none\",\"catcherLocalDomain\":[\"127.0.0.1\",\"localhost\",\"img.baidu.com\"],\"catcherActionName\":\"catchimage\",\"catcherFieldName\":\"source\",\"catcherPathFormat\":\"storage/image/{yyyy}{mm}{dd}/{time}\",\"catcherUrlPrefix\":\"//wode.cos.ap-chengdu.myqcloud.com/\",\"catcherMaxSize\":16777216,\"catcherAllowFiles\":[\".png\",\".jpg\",\".jpeg\",\".gif\",\".bmp\",\".PNG\",\".JPG\",\".JPEG\",\".GIF\",\".BMP\"],\"videoActionName\":\"uploadvideo\",\"videoFieldName\":\"upfile\",\"videoPathFormat\":\"storage/video/{yyyy}{mm}{dd}/{time}\",\"videoUrlPrefix\":\"//wode.cos.ap-chengdu.myqcloud.com/\",\"videoMaxSize\":16777216,\"videoAllowFiles\":[\".flv\",\".swf\",\".mkv\",\".avi\",\".rm\",\".rmvb\",\".mpeg\",\".mpg\",\".ogg\",\".ogv\",\".mov\",\".wmv\",\".mp4\",\".webm\",\".mp3\",\".wav\",\".mid\",\".FLV\",\".SWF\",\".MKV\",\".AVI\",\".RM\",\".RMVB\",\".MPEG\",\".MPG\",\".OGG\",\".OGV\",\".MOV\",\".WMV\",\".MP4\",\".WEBM\",\".MP3\",\".WAV\",\".MID\"],\"fileActionName\":\"uploadfile\",\"fileFieldName\":\"upfile\",\"filePathFormat\":\"storage/file/{yyyy}{mm}{dd}/{time}\",\"fileUrlPrefix\":\"//wode.cos.ap-chengdu.myqcloud.com/\",\"fileMaxSize\":16777216,\"fileAllowFiles\":[\".png\",\".jpg\",\".jpeg\",\".gif\",\".bmp\",\".flv\",\".swf\",\".mkv\",\".avi\",\".rm\",\".rmvb\",\".mpeg\",\".mpg\",\".ogg\",\".ogv\",\".mov\",\".wmv\",\".mp4\",\".webm\",\".mp3\",\".wav\",\".mid\",\".rar\",\".zip\",\".tar\",\".gz\",\".7z\",\".bz2\",\".cab\",\".iso\",\".doc\",\".docx\",\".xls\",\".xlsx\",\".ppt\",\".pptx\",\".pdf\",\".txt\",\".md\",\".xml\",\".PNG\",\".JPG\",\".JPEG\",\".GIF\",\".BMP\",\".FLV\",\".SWF\",\".MKV\",\".AVI\",\".RM\",\".RMVB\",\".MPEG\",\".MPG\",\".OGG\",\".OGV\",\".MOV\",\".WMV\",\".MP4\",\".WEBM\",\".MP3\",\".WAV\",\".MID\",\".RAR\",\".ZIP\",\".TAR\",\".GZ\",\".7Z\",\".BZ2\",\".CAB\",\".ISO\",\".DOC\",\".DOCX\",\".XLS\",\".XLSX\",\".PPT\",\".PPTX\",\".PDF\",\".TXT\",\".MD\",\".XML\"],\"imageManagerActionName\":\"listimage\",\"imageManagerListPath\":\"storage/image/\",\"imageManagerListSize\":20,\"imageManagerUrlPrefix\":\"//wode.cos.ap-chengdu.myqcloud.com/\",\"imageManagerInsertAlign\":\"none\",\"imageManagerAllowFiles\":[\".png\",\".jpg\",\".jpeg\",\".gif\",\".bmp\",\".PNG\",\".JPG\",\".JPEG\",\".GIF\",\".BMP\"],\"fileManagerActionName\":\"listfile\",\"fileManagerListPath\":\"storage/file/\",\"fileManagerUrlPrefix\":\"//wode.cos.ap-chengdu.myqcloud.com/\",\"fileManagerListSize\":20,\"fileManagerAllowFiles\":[\".png\",\".jpg\",\".jpeg\",\".gif\",\".bmp\",\".flv\",\".swf\",\".mkv\",\".avi\",\".rm\",\".rmvb\",\".mpeg\",\".mpg\",\".ogg\",\".ogv\",\".mov\",\".wmv\",\".mp4\",\".webm\",\".mp3\",\".wav\",\".mid\",\".rar\",\".zip\",\".tar\",\".gz\",\".7z\",\".bz2\",\".cab\",\".iso\",\".doc\",\".docx\",\".xls\",\".xlsx\",\".ppt\",\".pptx\",\".pdf\",\".txt\",\".md\",\".xml\",\".PNG\",\".JPG\",\".JPEG\",\".GIF\",\".BMP\",\".FLV\",\".SWF\",\".MKV\",\".AVI\",\".RM\",\".RMVB\",\".MPEG\",\".MPG\",\".OGG\",\".OGV\",\".MOV\",\".WMV\",\".MP4\",\".WEBM\",\".MP3\",\".WAV\",\".MID\",\".RAR\",\".ZIP\",\".TAR\",\".GZ\",\".7Z\",\".BZ2\",\".CAB\",\".ISO\",\".DOC\",\".DOCX\",\".XLS\",\".XLSX\",\".PPT\",\".PPTX\",\".PDF\",\".TXT\",\".MD\",\".XML\"]}";
            }
            else
            {
                return "";
            }

        }
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [CreateBy("孙泽伟")]
        [RequestSizeLimit(1000_000_000)]//最大100m左右
        [Route("/system/file/uploadform/wps")]
        public async Task<JsonResult> UploadFileFormWps([FromForm] FileUploadInput input)
        {
            OperateStatus<IList<SystemFile>> operateStatus = new OperateStatus<IList<SystemFile>>();
            try
            {
                LogWriter.Debug(input.Title);
                //判断是否已存在文件
                IList<SystemFile> instanceFiles = new List<SystemFile>();
                var files = Request.Form.Files;
                var basePath = $"/system/file/{input.UserId}";
                if (files.Any())
                {
                    var uploadPath = ConfigurationUtil.GetUploadPath();
                    //判断是否具有文件,

                    var fileCorrelations = await _systemFileLogic.FindAllAsync(f => f.CorrelationId == input.CorrelationId);
                    if (files.Count > 0)
                    {
                        foreach (var item in fileCorrelations)
                        {
                            //删除本地文件
                            FileUtil.DeleteFile(uploadPath + item.Path);
                            if (item.Extension == ".docx" || item.Extension == ".doc")
                            {
                                var pdfPath = item.Path.Replace(".docx", ".pdf").Replace(".doc", ".pdf");
                                FileUtil.DeleteFile(uploadPath + pdfPath);
                            }
                        }
                    }

                   
                    foreach (var file in files)
                    {
                        var fileId = CombUtil.NewComb();
                        if (!SystemIo.Directory.Exists(uploadPath + basePath+"/"+ fileId))
                        {
                            SystemIo.Directory.CreateDirectory(uploadPath + basePath + "/" + fileId);
                        }
                        var extension = SystemIo.Path.GetExtension(file.FileName);
                        string path = basePath + "/" + fileId + "/发文信息" + extension ;
                        string filename = uploadPath + path;
                        using (SystemIo.FileStream fs = SystemIo.File.Create(filename))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                        }
                        if (extension == ".docx" || extension == ".doc")
                        {
                            //判断是否为word，若为word转换为pdf
                            var doc = new Aspose.Words.Document(filename);
                            var pdfPath = filename.Replace(".docx", ".pdf").Replace(".doc", ".pdf");
                            doc.Save(pdfPath);

                            var filePdf = new SystemIo.FileInfo(pdfPath);
                            instanceFiles.Add(new SystemFile
                            {
                                FileId = fileId,
                                IsDelete = false,
                                Name = input.Title.IsNotNullOrEmpty() ? input.Title + extension : file.FileName,
                                Path = path,
                                Extension = extension,
                                Size = file.Length / 1024,
                                CreateTime = DateTime.Now,
                                CorrelationId = input.CorrelationId,
                                CreateUserId = input.UserId
                            });
                        }
                        else
                        {
                            instanceFiles.Add(new SystemFile
                            {
                                FileId = fileId,
                                IsDelete = false,
                                Name = file.FileName,
                                Path = path,
                                Extension = SystemIo.Path.GetExtension(file.FileName),
                                ContentType = file.ContentType,
                                Size = file.Length / 1024,
                                CreateTime = DateTime.Now,
                                CorrelationId = input.CorrelationId,
                                CreateUserId = input.UserId
                            });
                        }
                        await _systemFileLogic.DeleteFileByRelationId(new IdInput<string>(input.CorrelationId));
                        var result = await _systemFileLogic.UploadFile(instanceFiles);
                    }

                    return Json(new
                    {
                        code = 0,
                        data = new
                        {
                            fileId = instanceFiles[0].FileId,
                            url = instanceFiles[0].Path,
                        }
                    });

                }
                else
                {
                    operateStatus.Code = ResultCode.Success;
                    operateStatus.Msg = Chs.Successful;
                }
            }
            catch (Exception ex)
            {
                operateStatus.Msg = ex.Message;
            }
            return Json(operateStatus);
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [RequestSizeLimit(1000_000_000)]//最大100m左右
        [Route("/system/file/uploadform")]
        public async Task<JsonResult> UploadFileForm([FromForm] FileUploadInput input)
        {
            OperateStatus<IList<SystemFile>> operateStatus = new OperateStatus<IList<SystemFile>>();
            try
            {
                IList<SystemFile> instanceFiles = new List<SystemFile>();
                var files = Request.Form.Files;
                var basePath = $"/system/file/{CurrentUser.UserId}";
                if (files.Any())
                {
                    var uploadPath = ConfigurationUtil.GetUploadPath();
                    if (!SystemIo.Directory.Exists(uploadPath + basePath))
                    {
                        SystemIo.Directory.CreateDirectory(uploadPath + basePath);
                    }
                    foreach (var file in files)
                    {
                        var extension = SystemIo.Path.GetExtension(file.FileName);
                        string path = basePath + "/" + Guid.NewGuid() + extension;
                        string filename = uploadPath + path;
                        using (SystemIo.FileStream fs = SystemIo.File.Create(filename))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                        }
                        if (extension == ".docx" || extension == ".doc")
                        {
                            //判断是否为word，若为word转换为pdf
                            var doc = new Aspose.Words.Document(filename);
                            var pdfPath = filename.Replace(".docx", ".pdf").Replace(".doc", ".pdf");
                            path = pdfPath.Replace(uploadPath, "");
                            doc.Save(pdfPath);
                            FileUtil.DeleteFile(filename);

                            var filePdf = new SystemIo.FileInfo(pdfPath);
                            instanceFiles.Add(new SystemFile
                            {
                                IsDelete = false,
                                Name = file.FileName.Replace(".docx", ".pdf").Replace(".doc", ".pdf"),
                                Path = path,
                                Extension = ".pdf",
                                Size = filePdf.Length / 1024,
                                CreateTime = DateTime.Now,
                                CorrelationId = input.CorrelationId,
                                CreateUserId = CurrentUser.UserId
                            });
                        }
                        else
                        {
                            instanceFiles.Add(new SystemFile
                            {
                                IsDelete = false,
                                Name = file.FileName,
                                Path = path,
                                Extension = SystemIo.Path.GetExtension(file.FileName),
                                ContentType = file.ContentType,
                                Size = file.Length / 1024,
                                CreateTime = DateTime.Now,
                                CorrelationId = input.CorrelationId,
                                CreateUserId = CurrentUser.UserId
                            });
                        }

                        if (input.Single)
                        {
                            await _systemFileLogic.DeleteFileByRelationId(new IdInput<string>(input.CorrelationId));
                        }
                        var result = await _systemFileLogic.UploadFile(instanceFiles);
                    }

                    return Json(new
                    {
                        code = 0,
                        data = new
                        {
                            fileId = instanceFiles[0].FileId,
                            url = instanceFiles[0].Path,
                        }
                    });

                }
                else
                {
                    operateStatus.Code = ResultCode.Success;
                    operateStatus.Msg = Chs.Successful;
                }
            }
            catch (Exception ex)
            {
                operateStatus.Msg = ex.Message;
            }
            return Json(operateStatus);
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Route("/system/file/delete")]
        public async Task<JsonResult> DeleteFile(IdInput input)
        {
            return Json(await _systemFileLogic.DeleteFile(input));
        }

        /// <summary>
        /// 文件上传，返回文件存储的相对路径数组
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("文件上传，返回文件存储的相对路径数组", RemarkFrom.System)]
        [RequestSizeLimit(1000_000_000)]//最大100m左右
        [Route("/system/file/uploadreturnpath")]
        public async Task<JsonResult> UploadFileFormReurnPaths([FromForm] FileUploadInput input)
        {
            OperateStatus<List<SystemFile>> operateStatus = new OperateStatus<List<SystemFile>>();
            try
            {
                var files = Request.Form.Files;
                if (files.Any())
                {
                    string RelativePath = "Other";
                    if (!string.IsNullOrEmpty(input.RelativePath))
                        RelativePath = input.RelativePath;
                    RelativePath = "/" + RelativePath + (CurrentUser.UserId == Guid.Empty ? "" : "/" + CurrentUser.UserId.ToString());
                    var uploadPath = ConfigurationUtil.GetUploadPath();
                    string tempPath = uploadPath + RelativePath;
                    if (!SystemIo.Directory.Exists(tempPath))
                    {
                        SystemIo.Directory.CreateDirectory(tempPath);
                    }
                    List<SystemFile> filePathList = new List<SystemFile>();
                    IList<SystemFile> instanceFiles = new List<SystemFile>();
                    foreach (var file in files)
                    {
                        string fileName = Guid.NewGuid().ToString("N") + SystemIo.Path.GetExtension(file.FileName);
                        string filePath = tempPath + "/" + fileName;
                        using (SystemIo.FileStream fs = SystemIo.File.Create(filePath))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                        }
                        string RelativePathFile = "/" + RelativePath + "/" + fileName;
                        var fileId = CombUtil.NewComb();
                        filePathList.Add(new SystemFile
                        {
                            FileId = fileId,
                            Path = ConfigurationUtil.GetFileServer() + RelativePathFile
                        });
                        instanceFiles.Add(new SystemFile
                        {
                            FileId = fileId,
                            IsDelete = false,
                            Name = file.FileName,
                            Path = RelativePathFile,
                            Extension = SystemIo.Path.GetExtension(file.FileName),
                            ContentType = file.ContentType,
                            Size = file.Length / 1024,
                            CreateTime = DateTime.Now,
                            CorrelationId = input.CorrelationId,
                            CreateUserId = CurrentUser.UserId
                        });
                        if (input.Single)
                        {
                            await _systemFileLogic.DeleteFileByRelationId(new IdInput<string>(input.CorrelationId));
                        }
                        var result = await _systemFileLogic.UploadFile(instanceFiles);
                    }

                    operateStatus.Data = filePathList;
                    operateStatus.Code = ResultCode.Success;
                    operateStatus.Msg = Chs.Successful;
                }
                else
                {
                    operateStatus.Code = ResultCode.Success;
                    operateStatus.Msg = Chs.Successful;
                }
            }
            catch (Exception ex)
            {
                operateStatus.Msg = ex.Message;
            }
            return Json(operateStatus);
        }
        #endregion
    }
}
