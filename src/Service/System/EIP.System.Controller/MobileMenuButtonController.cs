using EIP.Base.Models.Entities.System;
using EIP.Common.Controller.Attribute;
using EIP.Common.Models.Dtos;
using EIP.System.Logic.Permission.ILogic;
using EIP.System.Models.Dtos.MobileMenu;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.Threading.Tasks;

namespace EIP.System.Controller
{
    /// <summary>
    /// 移动端按钮
    /// </summary>
    public class MobileMenuButtonController : BaseSystemController
    {
        #region 构造函数
        private readonly ISystemMobileMenuButtonLogic _systemMobileMenuButtonLogic;
        /// <summary>
        /// 移动端按钮构造函数
        /// </summary>
        /// <param name="systemMobileMenuButtonLogic"></param>
        public MobileMenuButtonController(ISystemMobileMenuButtonLogic systemMobileMenuButtonLogic)
        {
            _systemMobileMenuButtonLogic = systemMobileMenuButtonLogic;
        }

        #endregion

        #region 方法

        /// <summary>
        /// 根据模块Id获取模块按钮信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("界面按钮-方法-列表-根据模块Id获取按钮信息", RemarkFrom.System)]
        [Route("/system/mobilemenubutton/list")]
        public async Task<JsonResult> FindMenuButtonByMenuId(SystemMobileMenuFindButtonByMenuIdInput input)
        {
            return JsonForGridPaging(await _systemMobileMenuButtonLogic.FindMenuButtonByMenuId(input));
        }

        /// <summary>
        /// 根据Id获取
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Description("移动端按钮-方法-编辑-根据Id获取")]
        [Route("/system/mobilemenubutton/{id}")]
        public async Task<JsonResult> FindById([FromRoute] IdInput input)
        {
            return Json(await _systemMobileMenuButtonLogic.FindById(input));
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("移动端按钮-方法-编辑-保存")]
        [Route("/system/mobilemenubutton")]
        public async Task<JsonResult> Save(SystemMobileMenuButton input)
        {
            return Json(await _systemMobileMenuButtonLogic.Save(input));
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input">主键集合</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("移动端按钮-方法-列表-删除")]
        [Route("/system/mobilemenubutton/delete")]
        public async Task<JsonResult> Delete(IdInput<string> input)
        {
            return Json(await _systemMobileMenuButtonLogic.Delete(input));
        }

        /// <summary>
        /// 冻结
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("移动端按钮-方法-冻结", RemarkFrom.System)]
        [Route("/system/mobilemenubutton/isfreeze")]
        public async Task<JsonResult> IsFreeze(IdInput input)
        {
            return Json(await _systemMobileMenuButtonLogic.IsFreeze(input));
        }
        #endregion
    }
}
