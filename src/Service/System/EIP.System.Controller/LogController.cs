﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Entities.System;
using EIP.Common.Controller.Attribute;
using EIP.Common.Extension;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Dtos.Reports;
using EIP.Common.Models.Paging;
using EIP.System.Logic;
using EIP.System.Models.Dtos.Log;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EIP.System.Controller
{
    /// <summary>
    /// 日志管理控制器
    /// </summary>

    public class LogController : BaseSystemController
    {
        #region 构造函数
        private readonly ISystemExceptionLogLogic _exceptionLogLogic;
        private readonly ISystemLoginLogLogic _loginLogLogic;
        private readonly ISystemOperationLogLogic _operationLogLogic;
        private readonly ISystemRateLimitLogLogic _rateLimitLogLogic;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionLogLogic"></param>
        /// <param name="loginLogLogic"></param>
        /// <param name="operationLogLogic"></param>
        /// <param name="rateLimitLogLogic"></param>
        public LogController(ISystemExceptionLogLogic exceptionLogLogic,
            ISystemLoginLogLogic loginLogLogic,
            ISystemOperationLogLogic operationLogLogic,
            ISystemRateLimitLogLogic rateLimitLogLogic)
        {
            _operationLogLogic = operationLogLogic;
            _exceptionLogLogic = exceptionLogLogic;
            _loginLogLogic = loginLogLogic;
            _rateLimitLogLogic = rateLimitLogLogic;
        }

        #endregion

        #region 异常日志

        /// <summary>
        /// 获取所有异常信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("异常日志-方法-列表-获取所有异常信息", RemarkFrom.System)]
        [Route("/system/log/exception")]
        public async Task<JsonResult> FindPagingExceptionLog(SystemExceptionLogFindPagingInput paging)
        {
            return JsonForGridPaging(await _exceptionLogLogic.FindSystemExceptionLog(paging));
        }

        /// <summary>
        /// 根据主键获取异常明细
        /// </summary>
        /// <param name="input">主键Id</param>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("异常日志-方法-列表-根据主键获取异常明细", RemarkFrom.System)]
        [Route("/system/log/exception/{id}")]
        public async Task<JsonResult> FindExceptionLogById([FromRoute] IdInput<Guid> input)
        {
            return Json(OperateStatus<SystemExceptionLog>.Success(await _exceptionLogLogic.FindAsync(f => f.ExceptionLogId == input.Id)));
        }

        /// <summary>
        /// 根据主键删除异常信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("异常日志-方法-列表-根据主键删除异常信息", RemarkFrom.System)]
        [Route("/system/log/exception/{id}")]
        public async Task<JsonResult> DeleteExceptionLogById(IdInput<string> input)
        {
            return Json(await _exceptionLogLogic.DeleteByIdsAsync(input.Id));
        }

        /// <summary>
        /// 导出到Excel
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("异常日志-方法-列表-导出到Excel", RemarkFrom.System)]
        [Route("/system/log/exception/export")]
        public async Task<FileResult> ExportExcelToExceptionLog(QueryParam paging)
        {
            ExcelReportDto excelReportDto = new ExcelReportDto()
            {
                //TemplatePath = Server.MapPath("/") + "DataUser/Templates/System/Log/异常日志.xlsx",
                DownTemplatePath = "异常日志" + string.Format("{0:yyyyMMddHHmmssffff}", DateTime.Now) + ".xlsx",
                Title = "异常日志.xlsx"
            };
            await _exceptionLogLogic.ReportExcelExceptionLogQuery(paging, excelReportDto);
            //return File(new FileStream(excelReportDto.DownPath, FileMode.Open), "application/octet-stream", Server.UrlEncode(excelReportDto.Title));
            return null;
        }

        #endregion

        #region 登录日志

        /// <summary>
        /// 获取所有登录日志信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("登录日志-方法-列表-获取所有登录日志信息", RemarkFrom.System)]
        [Route("/system/log/login")]
        public async Task<JsonResult> FindPagingLoginLog(SystemLoginLogFindPagingInput paging)
        {
            paging.UserId = CurrentUser.UserId;
            return JsonForGridPaging(await _loginLogLogic.Find(paging));
        }

        /// <summary>
        /// 获取所有登录日志信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("登录日志-方法-列表-获取所有登录日志信息", RemarkFrom.System)]
        [Route("/system/log/login/{id}")]
        public async Task<JsonResult> FindLoginLogById([FromRoute] IdInput input)
        {
            return Json(await _loginLogLogic.FindById(input));
        }

        /// <summary>
        /// 获取所有登录日志信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("登录日志-方法-列表-获取所有登录日志分析", RemarkFrom.System)]
        [Route("/system/log/login/analysis")]
        public async Task<JsonResult> FindLoginLogAnalysis( SystemLoginLogFindPagingInput paging)
        {
            var datas = await _loginLogLogic.FindAnalysis(paging);
            IList<string> xdata = new List<string>();
            IList<int> ydata = new List<int>();
            if (datas.Data.Any())
            {
                int days = ((!paging.CreateTime.IsNullOrEmpty() ? Convert.ToDateTime(paging.EndCreateTime.ToString("yyyy-MM-dd")) : DateTime.Now) - Convert.ToDateTime(datas.Data.Min(m => m.CreateTime).ToString("yyyy-MM-dd"))).Days;
                for (int i = 0; i < days + 1; i++)
                {
                    var time = datas.Data.Min(m => m.CreateTime).AddDays(i);
                    time = Convert.ToDateTime(time.ToString("yyyy-MM-dd"));
                    xdata.Add(time.ToString("MM-dd"));
                    ydata.Add(datas.Data.Where(w => w.CreateTime >= time.AddDays(0).Date && w.CreateTime <= time.AddDays(1).Date).Count());
                }
            }
            return Json(new
            {
                analysis = new
                {
                    xdata,
                    ydata
                }
            });
        }

        /// <summary>
        /// 根据主键删除登录日志
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("异常日志-方法-列表-根据主键删除登录日志", RemarkFrom.System)]
        [Route("/system/log/login/{id}")]
        public async Task<JsonResult> DeleteLoginLogById(IdInput<string> input)
        {
            return Json(await _loginLogLogic.Delete(input));
        }

        /// <summary>
        /// 导出到Excel
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("登录日志-方法-列表-导出到Excel", RemarkFrom.System)]
        [Route("/system/log/login/export")]
        public async Task<FileResult> ExportExcelToLoginLog(QueryParam paging)
        {
            //ExcelReportDto excelReportDto = new ExcelReportDto()
            //{
            //    TemplatePath = Server.MapPath("/") + "DataUser/Templates/System/Log/登录日志.xlsx",
            //    DownTemplatePath = "登录日志" + string.Format("{0:yyyyMMddHHmmssffff}", DateTime.Now) + ".xlsx",
            //    Title = "登录日志.xlsx"
            //};
            //await _loginLogLogic.ReportExcelLoginLogQuery(paging, excelReportDto);
            //return File(new FileStream(excelReportDto.DownPath, FileMode.Open), "application/octet-stream", Server.UrlEncode(excelReportDto.Title));
            return null;
        }
        #endregion

        #region 操作日志

        /// <summary>
        /// 获取所有操作日志信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("操作日志-方法-列表-获取所有操作日志信息", RemarkFrom.System)]
        [Route("/system/log/operation")]
        public async Task<JsonResult> FindPagingOperationLog( SystemOperationLogFindPagingInput paging)
        {
            return JsonForGridPaging(await _operationLogLogic.FindPaging(paging));
        }

        /// <summary>
        /// 根据主键获取操作日志信息明细
        /// </summary>
        /// <param name="input">主键Id</param>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("操作日志-方法-列表-根据主键获取操作日志信息明细", RemarkFrom.System)]
        [Route("/system/log/operation/{id}")]
        public async Task<JsonResult> FindOperationLogById([FromRoute] IdInput input)
        {
            return Json(await _operationLogLogic.FindById(input));
        }

        /// <summary>
        /// 根据主键删除操作日志
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("异常日志-方法-列表-根据主键删除操作日志", RemarkFrom.System)]
        [Route("/system/log/operation/{id}")]
        public async Task<JsonResult> DeleteOperationLogById(IdInput<string> input)
        {
            return Json(await _operationLogLogic.Delete(input));
        }

        /// <summary>
        /// 导出到Excel
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("操作日志-方法-列表-导出到Excel", RemarkFrom.System)]
        [Route("/system/log/operation/export")]
        public async Task<FileResult> ExportExcelToOperationLog(QueryParam paging)
        {
            //ExcelReportDto excelReportDto = new ExcelReportDto()
            //{
            //    TemplatePath = Server.MapPath("/") + "DataUser/Templates/System/Log/操作日志.xlsx",
            //    DownTemplatePath = "操作日志" + string.Format("{0:yyyyMMddHHmmssffff}", DateTime.Now) + ".xlsx",
            //    Title = "操作日志.xlsx"
            //};
            //await _operationLogLogic.ReportExcelOperationLogQuery(paging, excelReportDto);
            //return File(new FileStream(excelReportDto.DownPath, FileMode.Open), "application/octet-stream", Server.UrlEncode(excelReportDto.Title));
            return null;

        }
        #endregion

        #region 限流日志

        /// <summary>
        /// 获取所有限流信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("限流日志-方法-列表-获取所有限流信息", RemarkFrom.System)]
        [Route("/system/log/ratelimit")]
        public async Task<JsonResult> FindPagingRateLimitLog(SystemRateLimitLogFindPagingInput input)
        {
            return JsonForGridPaging(await _rateLimitLogLogic.FindPagingRateLimitLog(input));
        }

        /// <summary>
        /// 根据主键获取限流明细
        /// </summary>
        /// <param name="input">主键Id</param>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("限流日志-方法-列表-根据主键获取限流明细", RemarkFrom.System)]
        [Route("/system/log/ratelimit/{id}")]
        public async Task<JsonResult> FindRateLimitLogById([FromRoute] IdInput<Guid> input)
        {
            return Json(OperateStatus<SystemRateLimitLog>.Success(await _rateLimitLogLogic.FindAsync(f => f.RateLimitLogId == input.Id)));
        }

        /// <summary>
        /// 根据主键删除限流信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("限流日志-方法-列表-根据主键删除限流信息", RemarkFrom.System)]
        [Route("/system/log/ratelimit/{id}")]
        public async Task<JsonResult> DeleteRateLimitLogById(IdInput<string> input)
        {
            return Json(await _rateLimitLogLogic.DeleteByIdsAsync(input.Id));
        }

        /// <summary>
        /// 导出到Excel
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("限流日志-方法-列表-导出到Excel", RemarkFrom.System)]
        [Route("/system/log/ratelimit/export")]
        public async Task<FileResult> ExportExcelToRateLimitLog(QueryParam paging)
        {
            ExcelReportDto excelReportDto = new ExcelReportDto()
            {
                //TemplatePath = Server.MapPath("/") + "DataUser/Templates/System/Log/限流日志.xlsx",
                DownTemplatePath = "限流日志" + string.Format("{0:yyyyMMddHHmmssffff}", DateTime.Now) + ".xlsx",
                Title = "限流日志.xlsx"
            };
            await _rateLimitLogLogic.ReportExcelRateLimitLogQuery(paging, excelReportDto);
            //return File(new FileStream(excelReportDto.DownPath, FileMode.Open), "application/octet-stream", Server.UrlEncode(excelReportDto.Title));
            return null;
        }

        #endregion
    }
}