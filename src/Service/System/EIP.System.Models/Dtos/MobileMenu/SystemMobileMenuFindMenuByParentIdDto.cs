﻿/**************************************************************
* Copyright (C) 2018 www.sf-info.cn 盛峰版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Entities.System;
using EIP.Common.Models.Paging;
using System;

namespace EIP.System.Models.Dtos.MobileMenu
{
    /// <summary>
    /// 根据父级查询模块
    /// </summary>
    public class SystemMobileMenuFindInput : QueryParam
    {
        /// <summary>
        /// 模块Id
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// 是否包含本身
        /// </summary>
        public bool HaveSelf { get; set; } = true;
    }

    /// <summary>
    /// 根据父级获取模块
    /// </summary>
    public class SystemMobileMenuFindOutput
    {
        /// <summary>
        /// 主键id
        /// </summary>		
        public Guid MobileMenuId { get; set; }

        /// <summary>
        /// 
        /// </summary>		
        public string ParentName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>		
        public string Name { get; set; }

        /// <summary>
        /// 图标
        /// </summary>		
        public string Icon { get; set; }

        /// <summary>
        /// 打开类型
        /// </summary>		
		public byte? OpenType { get; set; }

        /// <summary>
        /// 
        /// </summary>		
        public string Path { get; set; }

        /// <summary>
        /// 
        /// </summary>		
        public string Remark { get; set; }

        /// <summary>
        /// 
        /// </summary>		
        public int OrderNo { get; set; }

        /// <summary>
        /// 
        /// </summary>		
        public bool HaveMenuPermission { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>		
        public bool HaveDataPermission { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>		
        public bool HaveFieldPermission { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>		
        public bool HaveButtonPermission { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>		
        public bool IsFreeze { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>
        public string ParentIdsName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 创建人员名称
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>		
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 修改人员名称
        /// </summary>
        public string UpdateUserName { get; set; }
    }
}