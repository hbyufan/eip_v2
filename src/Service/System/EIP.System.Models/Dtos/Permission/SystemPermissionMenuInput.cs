﻿using System;

namespace EIP.System.Models.Dtos.Permission
{
    /// <summary>
    /// 
    /// </summary>
    public class SystemPermissionMenuInput
    {
        /// <summary>
        /// 当前用户
        /// </summary>
        public Guid UserId { get; set; }
    }
}