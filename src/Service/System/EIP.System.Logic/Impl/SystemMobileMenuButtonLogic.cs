using EIP.Base.Models.Entities.System;
using EIP.Base.Repository.Fixture;
using EIP.Common.Core.Context;
using EIP.Common.Extension;
using EIP.Common.Language.Resource;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Paging;
using EIP.Common.Util;
using EIP.System.Logic.Permission.ILogic;
using EIP.System.Models.Dtos.MobileMenu;
using EIP.System.Models.Enums;
using EIP.System.Repository.IRepository;
using System;
using System.Threading.Tasks;

namespace EIP.System.Logic.Permission.Logic
{
    /// <summary>
    /// 移动端按钮
    /// </summary>
    public class SystemMobileMenuButtonLogic : DapperAsyncLogic<SystemMobileMenuButton>, ISystemMobileMenuButtonLogic
    {
        #region 构造函数

        private readonly ISystemMobileMenuButtonRepository _systemMobileMenuButtonRepository;
        public SystemMobileMenuButtonLogic(ISystemMobileMenuButtonRepository systemMobileMenuButtonRepository)
        {
            _systemMobileMenuButtonRepository = systemMobileMenuButtonRepository;
        }

        #endregion

        #region 方法

        /// <summary>
        /// 保存功能项信息
        /// </summary>
        /// <param name="input">功能项信息</param>
        /// <returns></returns>
        public async Task<OperateStatus> Save(SystemMobileMenuButton input)
        {
            var currentUser = EipHttpContext.CurrentUser();
            var mobileMenuButton = await FindAsync(f => f.MobileMenuButtonId == input.MobileMenuButtonId);
            if (mobileMenuButton==null)
            {
                input.MobileMenuButtonId = CombUtil.NewComb();
                input.CreateTime = DateTime.Now;
                input.CreateUserId = currentUser.UserId;
                input.CreateUserName = currentUser.Name;
                input.UpdateTime = DateTime.Now;
                input.UpdateUserId = currentUser.UserId;
                input.UpdateUserName = currentUser.Name;
                return await InsertAsync(input);
            }

            input.Id = mobileMenuButton.Id;
            input.CreateTime = mobileMenuButton.CreateTime;
            input.CreateUserId = mobileMenuButton.CreateUserId;
            input.CreateUserName = mobileMenuButton.CreateUserName;

            input.UpdateTime = DateTime.Now;
            input.UpdateUserId = currentUser.UserId;
            input.UpdateUserName = currentUser.Name;
            return await UpdateAsync(input);
        }

        /// <summary>
        /// 删除功能项
        /// </summary>
        /// <param name="input">功能项信息</param>
        /// <returns></returns>
        public async Task<OperateStatus> Delete(IdInput<string> input)
        {
            var operateStatus = new OperateStatus();
            using (var fixture = new SqlDatabaseFixture(false))
            {
                var trans = fixture.Db.BeginTransaction();
                try
                {
                    foreach (var id in input.Id.Split(','))
                    {
                        var accessValue = Guid.Parse(id);
                        var privilegeAccess = EnumPrivilegeAccess.移动端模块按钮.ToShort();
                        await fixture.Db.SystemPermission.DeleteAsync(f => f.PrivilegeAccess == privilegeAccess && f.PrivilegeAccessValue == accessValue, trans);
                        await fixture.Db.SystemMobileMenuButton.DeleteAsync(d=>d.MobileMenuButtonId== accessValue, trans);
                    }
                    operateStatus.Msg = Chs.Successful;
                    operateStatus.Code = ResultCode.Success;
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    operateStatus.Msg = ex.Message;
                }
            }
            return operateStatus;
        }

        /// <summary>
        /// 根据模块获取功能项信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<PagedResults<SystemMobileMenuFindButtonByMenuIdOutput>>> FindMenuButtonByMenuId(SystemMobileMenuFindButtonByMenuIdInput input)
        {
            return OperateStatus<PagedResults<SystemMobileMenuFindButtonByMenuIdOutput>>.Success(await _systemMobileMenuButtonRepository.FindMenuButtonByMenuId(input));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<SystemMobileMenuButton>> FindById(IdInput input)
        {
            return OperateStatus<SystemMobileMenuButton>.Success(await FindAsync(f => f.MobileMenuButtonId == input.Id));
        }

        /// <summary>
        /// 冻结
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus> IsFreeze(IdInput input)
        {
            var data = await FindAsync(f => f.MobileMenuButtonId == input.Id);
            data.IsFreeze = !data.IsFreeze;
            return await UpdateAsync(data);
        }
        #endregion
    }
}
