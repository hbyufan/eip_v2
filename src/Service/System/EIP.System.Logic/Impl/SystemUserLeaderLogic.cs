/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Entities.System;
using EIP.Base.Repository.Fixture;
using EIP.Common.Extension;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.System.Models.Dtos.User;
using EIP.System.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EIP.System.Logic.Impl
{
    /// <summary>
    /// 用户业务逻辑实现
    /// </summary>
    public class SystemUserLeaderLogic : DapperAsyncLogic<SystemUserLeader>, ISystemUserLeaderLogic
    {
        private readonly ISystemUserInfoRepository _userInfoRepository;

        /// <summary>
        /// 系统人员
        /// </summary>
        /// <param name="userInfoRepository"></param>
        public SystemUserLeaderLogic(ISystemUserInfoRepository userInfoRepository)
        {
            _userInfoRepository = userInfoRepository;
        }
        /// <summary>
        /// 根据Id获取用户领导
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemUserChosenOutput>>> GetUserLeader(SystemUserChosenInput input)
        {
            //获取所有人员信息
            var chosenUserDtos = (await _userInfoRepository.FindAllUser(input.DataSql)).ToList();
            //获取所有上级
            var allLeaderUser = (await FindAllAsync(f => f.UserId == input.UserId)).ToList();
            foreach (var dto in chosenUserDtos)
            {
                dto.Exist = allLeaderUser.Where(w => w.LeaderUserId == dto.UserId).FirstOrDefault() != null;
            }
            return OperateStatus<IEnumerable<SystemUserChosenOutput>>.Success(chosenUserDtos.OrderByDescending(w => w.Exist));
        }

        /// <summary>
        /// 根据Id获取用户下级
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemUserChosenOutput>>> FindSubordinate(SystemUserChosenInput input)
        {
            //获取所有人员信息
            var chosenUserDtos = (await _userInfoRepository.FindAllUser(input.DataSql)).ToList();
            //获取所有上级
            var allLeaderUser = (await FindAllAsync(f => f.LeaderUserId == input.UserId)).ToList();
            foreach (var dto in chosenUserDtos)
            {
                dto.Exist = allLeaderUser.Where(w => w.UserId == dto.UserId).FirstOrDefault() != null;
            }
            return OperateStatus<IEnumerable<SystemUserChosenOutput>>.Success(chosenUserDtos.OrderByDescending(w => w.Exist));
        }

        /// <summary>
        /// 查询下级人员
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IList<SystemUserLeadersOutput>>> FindSubordinate(IdInput input)
        {
            using (var fixture = new SqlDatabaseFixture())
            {
                var data = (await fixture.Db.SystemUserFindSubordinateOutput.FindAllAsync<SystemUserLeadersOutput>(f => f.LeaderUserId == input.Id, o => o.Outputs)).ToList();
                if (data.Any())
                {
                    var subordinate = data.First().Outputs;
                    if (subordinate.Any())
                    {
                        var allOrgs = (await fixture.Db.SystemOrganization.FindAllAsync()).ToList();
                        foreach (var user in subordinate)
                        {
                            var organization = allOrgs.FirstOrDefault(w => w.OrganizationId == user.OrganizationId);
                            if (organization != null && !organization.ParentIds.IsNullOrEmpty())
                            {
                                foreach (var parent in organization.ParentIds.Split(','))
                                {
                                    //查找上级
                                    var dicinfo = allOrgs.FirstOrDefault(w => w.OrganizationId.ToString() == parent);
                                    if (dicinfo != null) user.OrganizationNames += dicinfo.Name + "/";
                                }
                                if (!user.OrganizationNames.IsNullOrEmpty())
                                    user.OrganizationNames = user.OrganizationNames.TrimEnd('/');
                            }
                        }
                    }
                    return OperateStatus<IList<SystemUserLeadersOutput>>.Success(subordinate);
                }
                return OperateStatus<IList<SystemUserLeadersOutput>>.Success(new List<SystemUserLeadersOutput>());
            }
        }

        /// <summary>
        /// 保存用户领导信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userLeaders"></param>
        /// <returns></returns>
        public async Task<OperateStatus> Save(Guid userId, IList<Guid> userLeaders)
        {
            //先删除所有领导数据
            OperateStatus operateStatus = new OperateStatus();
            try
            {
                await DeleteAsync(d => d.UserId == userId);
                //新增
                foreach (var item in userLeaders)
                {
                    operateStatus = await InsertAsync(new SystemUserLeader { UserId = userId, LeaderUserId = item });
                }
            }
            catch (Exception ex)
            {
                operateStatus.Msg = ex.Message;
            }
            return operateStatus;
        }

        /// <summary>
        /// 保存用户下级信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userSubordinate"></param>
        /// <returns></returns>
        public async Task<OperateStatus> SaveSubordinate(Guid userId, IList<Guid> userSubordinate)
        {
            //先删除所有领导数据
            OperateStatus operateStatus = new OperateStatus();
            try
            {
                await DeleteAsync(d => d.LeaderUserId == userId);
                //新增
                foreach (var item in userSubordinate)
                {
                    operateStatus = await InsertAsync(new SystemUserLeader { UserId = item, LeaderUserId = userId });
                }
            }
            catch (Exception ex)
            {
                operateStatus.Msg = ex.Message;
            }
            return operateStatus;
        }
    }
}