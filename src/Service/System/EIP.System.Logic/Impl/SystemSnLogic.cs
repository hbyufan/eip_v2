/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Agile.Models.Dtos.Sn;
using EIP.Base.Models.Entities.System;
using EIP.Base.Repository.Fixture;
using EIP.Common.Language.Resource;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Util;
using System;
using System.Threading.Tasks;

namespace EIP.System.Logic.Impl
{
    /// <summary>
    /// 
    /// </summary>
    public class SystemSnLogic : DapperAsyncLogic<SystemSn>, ISystemSnLogic
    {
        public async Task<OperateStatus<SystemSnOutput>> Find(SystemSnInput input)
        {
            OperateStatus<SystemSnOutput> operateStatus = new OperateStatus<SystemSnOutput>();
            using (var fix = new SqlDatabaseFixture())
            {
                var serialNo = await fix.Db.SystemSn.FindAsync(f => f.Code == input.Code);

                if (serialNo == null)
                {
                    serialNo = new SystemSn();
                    serialNo.SnId = CombUtil.NewComb();
                    serialNo.Code = input.Code;
                    serialNo.Number = 0;

                    input.Code = input.Code.Replace("{年}", DateTime.Now.ToString("yyyy"));
                    input.Code = input.Code.Replace("{月}", DateTime.Now.ToString("MM"));
                    input.Code = input.Code.Replace("{日}", DateTime.Now.ToString("dd"));
                    input.Code = input.Code.Replace("{时}", DateTime.Now.ToString("HH"));
                    input.Code = input.Code.Replace("{分}", DateTime.Now.ToString("mm"));
                    input.Code = input.Code.Replace("{秒}", DateTime.Now.ToString("ss"));
                    input.Code = input.Code.Replace("{毫秒}", DateTime.Now.ToString("fff"));
                    if (input.Code.Contains("{0"))
                    {
                        var snSplits = input.Code.Split("{");
                        var snLast = snSplits[1].Split("}");
                        var length = snLast[0].Length;
                        var snCount = (serialNo.Number + 1).ToString().PadLeft(length, '0');
                        input.Code = input.Code.Replace("{" + snSplits[1], snCount)+snLast[1];
                        serialNo.Number = 1;
                    }
                    serialNo.Value = input.Code;
                    await fix.Db.SystemSn.InsertAsync(serialNo);
                }
                else
                {
                    input.Code = input.Code.Replace("{年}", DateTime.Now.ToString("yyyy"));
                    input.Code = input.Code.Replace("{月}", DateTime.Now.ToString("MM"));
                    input.Code = input.Code.Replace("{日}", DateTime.Now.ToString("dd"));
                    input.Code = input.Code.Replace("{时}", DateTime.Now.ToString("HH"));
                    input.Code = input.Code.Replace("{分}", DateTime.Now.ToString("mm"));
                    input.Code = input.Code.Replace("{秒}", DateTime.Now.ToString("ss"));
                    input.Code = input.Code.Replace("{毫秒}", DateTime.Now.ToString("fff"));

                    if (input.Code.Contains("{0"))
                    {
                        var snSplits = input.Code.Split("{");
                        //是否从0开始
                        if (!serialNo.Value.Contains(snSplits[0]))
                        {
                            serialNo.Number = 0;
                        }
                        //有多少个0
                        var snLast = snSplits[1].Split("}");
                        var length = snLast[0].Length;
                        var snCount = (serialNo.Number + 1).ToString().PadLeft(length, '0');
                        input.Code = input.Code.Replace("{" + snSplits[1], snCount) + snLast[1];
                        serialNo.Number += 1;
                    }
                    serialNo.Value = input.Code;
                    await fix.Db.SystemSn.UpdateAsync(serialNo);
                }
                operateStatus.Code = ResultCode.Success;
                operateStatus.Msg = Chs.Successful;
                operateStatus.Data = new SystemSnOutput
                {
                    Value = serialNo.Value
                };
            }
            return operateStatus;
        }
    }
}