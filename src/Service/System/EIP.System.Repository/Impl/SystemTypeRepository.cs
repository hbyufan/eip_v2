/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/

using EIP.Base.Models.Entities.System;
using EIP.Common.Extension;
using EIP.Common.Models.Paging;
using EIP.Common.Repository;
using EIP.System.Models.Dtos.Login;
using EIP.System.Models.Dtos.Type;
using EIP.System.Models.Dtos.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EIP.System.Repository.Impl
{
    /// <summary>
    /// 
    /// </summary>
    public class SystemTypeRepository : ISystemTypeRepository
    {
        /// <summary>
        /// 复杂查询分页方式
        /// </summary>
        /// <param name="input">查询参数</param>
        /// <returns>分页</returns>
        public Task<PagedResults<SystemType>> Find(SystemTypeFindInput input)
        {
            var sql = new StringBuilder("SELECT *,@rowNumber, @recordCount FROM System_Type @where ");
            
            if (input.Sidx.IsNullOrEmpty())
            {
                input.Sidx = " OrderNo ";
            }
            return new SqlMapperUtil().PagingQuerySqlAsync<SystemType>(sql.ToString(), input);
        }
    }
}