/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Extension;
using EIP.Common.Models.Paging;
using EIP.Common.Models.Tree;
using EIP.Common.Repository;
using EIP.System.Models.Dtos.Organization;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EIP.System.Repository.Impl
{
    /// <summary>
    /// 组织机构
    /// </summary>
    public class SystemOrganizationRepository : ISystemOrganizationRepository
    {
        /// <summary>
        /// 获取数据权限
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<IEnumerable<BaseTree>> FindDataPermissionTree(SystemOrganizationDataPermissionInput input)
        {
            var sql = new StringBuilder();
            sql.Append($"SELECT OrganizationId id,ParentId parent,Name text,ParentIds parents FROM System_Organization org WHERE 1=1 AND {input.DataSql} ORDER BY OrderNo");
            return new SqlMapperUtil().SqlWithParams<BaseTree>(sql.ToString());
        }

        /// <summary>
        /// 根据父级查询下级
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<PagedResults<SystemOrganizationOutput>> FindByParentId(SystemOrganizationDataPermissionInput input)
        {
            var sql = new StringBuilder();
            sql.Append("select *,@rowNumber,@recordCount from System_Organization org @where ");

            if (input.Id != null)
            {
                sql.Append(input.HaveSelf
                    ? "AND org.ParentIds  like '%" + input.Id + "%'"
                    : "AND org.ParentIds  like '%" + input.Id + ",%'");
            }
            if (!input.DataSql.IsNullOrEmpty())
            {
                sql.Append("AND " + input.DataSql);
            }
            if (input.Sidx.IsNullOrEmpty())
            {
                input.Sidx = " org.OrderNo";
            }
            sql.Append(input.Sql);
            return new SqlMapperUtil().PagingQuerySqlAsync<SystemOrganizationOutput>(sql.ToString(),input);
        }
    }
}