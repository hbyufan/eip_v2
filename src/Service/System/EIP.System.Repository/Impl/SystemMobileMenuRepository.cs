using EIP.Common.Models.Paging;
using EIP.Common.Repository;
using EIP.System.Models.Dtos.MobileMenu;
using System.Text;
using System.Threading.Tasks;

namespace EIP.System.Repository.IRepository.Impl
{
    /// <summary>
    /// 移动端菜单
    /// </summary>
    public class SystemMobileMenuRepository : ISystemMobileMenuRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<PagedResults<SystemMobileMenuFindOutput>> Find(SystemMobileMenuFindInput input)
        {
            var sql = new StringBuilder();
            sql.Append("select menu.CreateTime,menu.CreateUserName,menu.UpdateTime,menu.UpdateUserName, menu.MobileMenuId,menu.ParentName,menu.Name,menu.Icon,menu.OpenType,menu.Path,menu.Remark,menu.OrderNo,menu.HaveMenuPermission,menu.HaveDataPermission,menu.HaveFieldPermission," +
                "menu.HaveButtonPermission, menu.IsFreeze,menu.ParentIdsName," +
                "@rowNumber, @recordCount from System_MobileMenu menu @where  ");
            if (input.Id.HasValue)
            {
                sql.Append(input.HaveSelf
                     ? "and menu.ParentIds like '%" + input.Id + "%'"
                     : "and menu.ParentIds like '%" + (input.Id + ",%'"));
            }
            if (input.Sidx.IsNullOrEmpty())
            {
                input.Sidx = " menu.OrderNo";
            }
            return new SqlMapperUtil().PagingQuerySqlAsync<SystemMobileMenuFindOutput>(sql.ToString(), input);
        }
    }
}
