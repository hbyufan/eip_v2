using EIP.Common.Models.Dtos;
using EIP.Common.Models.Paging;
using EIP.System.Models.Dtos.MobileMenu;
using EIP.System.Models.Dtos.MobileMenuButton;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EIP.System.Repository.IRepository
{
    /// <summary>
    /// 移动端按钮
    /// </summary>
    public interface ISystemMobileMenuButtonRepository
    {
        /// <summary>
        /// 根据模块获取功能项信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PagedResults<SystemMobileMenuFindButtonByMenuIdOutput>> FindMenuButtonByMenuId(SystemMobileMenuFindButtonByMenuIdInput input);

        /// <summary>
        /// 根据模块获取功能项信息
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        Task<IEnumerable<SystemMobileMenuButtonOutput>> FindMenuButtonByMenuId(IList<Guid> menuId = null);

        /// <summary>
        /// 根据模块获取功能项信息
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<SystemMobileMenuButtonOutput>> FindHaveMenuButtonPermission(IdInput input);

        /// <summary>
        /// 根据模块Id和用户Id获取按钮权限数据
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<SystemMobileMenuButtonFindOutput>> FindMobileMenuButtonByMenuIdAndUserId(SystemMobileMenuButtonFindInput input);
    }
}
