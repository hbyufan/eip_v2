﻿namespace EIP.Agile.Models.Dtos.Config
{
    /// <summary>
    /// 
    /// </summary>
    public class AgileConfigFindFormColumnsJsonOutput
    {
        /// <summary>
        /// 字段名称
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// 类型:子表subtable
        /// </summary>
        public string Type { get; set; }

    }
}
