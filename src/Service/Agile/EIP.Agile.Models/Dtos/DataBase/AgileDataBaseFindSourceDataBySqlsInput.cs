﻿using System;
using System.Collections.Generic;

namespace EIP.Agile.Models.Dtos.DataBase
{
    /// <summary>
    /// 
    /// </summary>
    public class AgileDataBaseFindSourceDataBySqlsInput
    {
        /// <summary>
        /// Sql集合
        /// </summary>
        public IList<string> Sqls { get; set; }

        /// <summary>
        /// 字符串
        /// </summary>
        public  string SqlsString { get; set; }

        /// <summary>
        /// 库Id
        /// </summary>
        public Guid DataBaseId { get; set; }

    }
}
