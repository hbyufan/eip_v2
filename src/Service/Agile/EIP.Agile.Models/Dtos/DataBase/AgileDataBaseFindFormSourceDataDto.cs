﻿using EIP.Common.Models.Paging;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace EIP.Agile.Models.Dtos.DataBase
{
    /// <summary>
    /// 
    /// </summary>
    public class AgileDataBaseFindFormSourceDataInput : QueryParam
    {
        /// <summary>
        /// 是否分页
        /// </summary>
        public bool IsPage { get; set; } = false;

        /// <summary>
        /// 表名称
        /// </summary>
        public string Table { get; set; }

        /// <summary>
        /// 键
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AgileDataBaseFindFormSourceDataOutput : object
    {
        /// <summary>
        /// 总数
        /// </summary>
        [JsonIgnore]
        [NotMapped]
        public long RecordCount { get; set; }
    }
}