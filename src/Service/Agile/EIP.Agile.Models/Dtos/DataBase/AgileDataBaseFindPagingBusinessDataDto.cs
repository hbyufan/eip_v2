﻿/**************************************************************
* Copyright (C) 2018 www.sf-info.cn 盛峰版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/11/15 15:23:03
* 文件名: SystemCodeGenerationFindPagingBusinessData
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Models.Paging;
using System;
using System.Collections.Generic;
using EIP.Common.Models;

namespace EIP.Agile.Models.Dtos.DataBase
{
    /// <summary>
    /// 继承分页参数
    /// </summary>
    public class AgileDataBaseFindPagingBusinessDataInput : QueryParam
    {
        /// <summary>
        /// 代码生成Id
        /// </summary>
        public Guid ConfigId { get; set; }

        /// <summary>
        /// 是否分页
        /// </summary>
        public bool IsPaging { get; set; }

        /// <summary>
        /// 路由信息
        /// </summary>
        public ViewRote Rote { get; set; }

        /// <summary>
        /// 是否具有数据权限
        /// </summary>
        public bool HaveDataPermission { get; set; }

        /// <summary>
        /// 导出列
        /// </summary>
        public IList<AgileDataBaseReportFindBusinessDataColsInput> Cols { get; set; } = new List<AgileDataBaseReportFindBusinessDataColsInput>();

        /// <summary>
        /// 导出文件名称
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MvcString { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ColsString { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class AgileDataBaseReportFindBusinessDataColsInput
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Field { get; set; }
    }
    /// <summary>
    /// 所有列表
    /// </summary>
    public class AgileDataBaseFindPagingBusinessDataTableInput
    {
        /// <summary>
        /// 列
        /// </summary>
        public IList<AgileDataBaseFindPagingBusinessDataTableColunmsInput> Columns { get; set; }
    }

    /// <summary>
    /// 列表名
    /// </summary>
    public class AgileDataBaseFindPagingBusinessDataTableColunmsInput
    {
        /// <summary>
        /// 列名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 是否需要查询
        /// </summary>
        public string IsSearch { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public string Type { get; set; }
    }
}
