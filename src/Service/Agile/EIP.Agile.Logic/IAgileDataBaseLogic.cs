﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EasyCaching.Core.Interceptor;
using EIP.Agile.Models.Dtos.DataBase;
using EIP.Common.Models;
using EIP.Common.Models.Dtos.DataBase;
using EIP.Common.Models.Tree;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EIP.Agile.Logic
{
    /// <summary>
    /// 数据库操作
    /// </summary>
    public interface IAgileDataBaseLogic
    {
        /// <summary>
        /// 获取所有应用数据库
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<IEnumerable<BaseTree>>> FindTableTree();

        /// <summary>
        /// 查看对应数据库空间占用情况
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<IEnumerable<AgileDataBaseSpaceOutput>>> FindDataBaseSpaceused();

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<IEnumerable<AgileDataBaseTableDto>>> FindDataBaseTable();

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<IEnumerable<AgileDataBaseTableDto>>> FindDataBaseView();
        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<IEnumerable<AgileDataBaseTableDto>>> FindDataBaseProc();
        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<IEnumerable<AgileDataBaseTableDto>>> FindDataBaseWorkflowTables();

        /// <summary>
        /// 获取对应表列信息
        /// </summary>
        /// <param name="doubleWayDto"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<IEnumerable<AgileDataBaseColumnDto>>> FindDataBaseColumns(AgileDataBaseTableDto doubleWayDto);

        /// <summary>
        /// 获取对应表列信息
        /// </summary>
        /// <param name="doubleWayDto"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<IEnumerable<AgileDataBaseColumnDto>>> FindWorkflowDataBaseColumnsList(AgileDataBaseTableDto doubleWayDto);

        /// <summary>
        /// 获取外键信息
        /// </summary>
        /// <param name="doubleWayDto"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<IEnumerable<AgileDataBaseFkColumnOutput>>> FindDataBasefFkColumn(AgileDataBaseTableDto doubleWayDto);

        /// <summary>
        /// 表是否存在
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<bool>> IsTableExist(AgileDataBaseIsTableExistInput input);

        /// <summary>
        /// 创建表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        OperateStatus SaveFormTable(AgileDataBaseSaveFormTableInput input);

        /// <summary>
        /// 修改表字段
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus> SaveFormTableField(AgileDataBaseSaveFormTableFieldInput input);

        /// <summary>
        /// 保存表单数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus> SaveBusinessData(AgileDataBaseSaveBusinessDataInput input);

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<object>> FindBusinessDataById(AgileDataBaseFindBusinessDataByIdInput input);

        /// <summary>
        /// 获取业务数据
        /// </summary>
        /// <param name="paging"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<dynamic>> FindBusinessData(AgileDataBaseFindPagingBusinessDataInput paging);

        /// <summary>
        /// 删除业务数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus> DeleteBusinessData(AgileDataBaseDeleteBusinessDataInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        Task<OperateStatus<dynamic>> FindFromSubTable(DataBaseSubTableDto input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        OperateStatus<dynamic> FindFormSourceData(AgileDataBaseFindFormSourceDataInput input);

        /// <summary>
        /// 查询业务数据分页
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileDataBaseLogic_Cache")]
        OperateStatus<dynamic> FindFormSourceDataPaging(AgileDataBaseFindFormSourceDataInput input);
    }
}