﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Agile.Models.Dtos.DataBase;
using EIP.Base.Models.Entities.Agile;
using EIP.Common.Logic;
using EIP.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EIP.Agile.Logic
{
    /// <summary>
    /// 数据来源操作
    /// </summary>
    public interface IAgileDataSourceLogic : IAsyncLogic<AgileDataSource>
    {
        /// <summary>
        /// 查看对应数据库空间占用情况
        /// </summary>
        /// <returns></returns>
        Task<OperateStatus<IEnumerable<AgileDataBaseSpaceOutput>>> FindDataBaseSpaceused();

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <returns></returns>
        Task<OperateStatus<IEnumerable<AgileDataBaseTableDto>>> FindDataBaseTable();

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<OperateStatus<IEnumerable<AgileDataBaseTableDto>>> FindDataBaseView();

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<OperateStatus<IEnumerable<AgileDataBaseTableDto>>> FindDataBaseProc();

        /// <summary>
        /// 获取对应表列信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<OperateStatus<IEnumerable<AgileDataBaseColumnDto>>> FindDataBaseColumns(AgileDataBaseTableDto input);

        /// <summary>
        /// 获取外键信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<OperateStatus<IEnumerable<AgileDataBaseFkColumnOutput>>> FinddatabsefFkColumn(AgileDataBaseTableDto input);

    }
}