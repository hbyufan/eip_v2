﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/

using EIP.Agile.Logic;

namespace EIP.Agile.Controller
{
    /// <summary>
    /// 数据源
    /// </summary>
    public class DataSourceController : BaseAgileController
    {
        #region 构造函数
        private readonly IAgileDataSourceLogic _systemDataSourceLogic;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="systemDataSourceLogic"></param>
        public DataSourceController(IAgileDataSourceLogic systemDataSourceLogic)
        {
            _systemDataSourceLogic = systemDataSourceLogic;
        }

        #endregion

        #region 方法
       

        #endregion
    }
}