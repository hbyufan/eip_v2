/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/

using Dapper;
using EIP.Agile.Models.Dtos.DataBase;
using EIP.Common.Extension;
using EIP.Common.Models.Resx;
using EIP.Common.Repository;
using EIP.Common.Util;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace EIP.Agile.Repository.Impl
{
    /// <summary>
    /// 数据来源
    /// </summary>
    public class AgileDataSourceRepository : IAgileDataSourceRepository
    {
        private string connectionString = ConfigurationUtil.GetSection("EIP:ConnectionString");
        private string connectionType = ConfigurationUtil.GetSection("EIP:ConnectionType").ToLower();

        #region 外键Sql
        const string FkSql = @"SELECT
                    ThisTable  = FK.TABLE_NAME,
                    ThisColumn = CU.COLUMN_NAME,
                    OtherTable  = PK.TABLE_NAME,
                    OtherColumn = PT.COLUMN_NAME, 
                    Constraint_Name = C.CONSTRAINT_NAME,
                    Owner = FK.TABLE_SCHEMA
                FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS C
                INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS FK ON C.CONSTRAINT_NAME = FK.CONSTRAINT_NAME
                INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS PK ON C.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME
                INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU ON C.CONSTRAINT_NAME = CU.CONSTRAINT_NAME
                INNER JOIN
                    (	
                        SELECT i1.TABLE_NAME, i2.COLUMN_NAME
                        FROM  INFORMATION_SCHEMA.TABLE_CONSTRAINTS i1
                        INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE i2 ON i1.CONSTRAINT_NAME = i2.CONSTRAINT_NAME
                        WHERE i1.CONSTRAINT_TYPE = 'PRIMARY KEY'
                    ) 
                PT ON PT.TABLE_NAME = PK.TABLE_NAME
                WHERE FK.Table_NAME=@tableName OR PK.Table_NAME=@tableName";
        #endregion

        #region 表Sql
        const string MssqlTableSql = @"SELECT tbs.name [Table],ds.value [Description] ,crdate CreateTime FROM  sysobjects tbs LEFT JOIN  sys.extended_properties ds ON ds.major_id=tbs.id AND ds.minor_id=0 WHERE   xtype='U' ORDER BY tbs.name";
        const string MysqlTableSql = @"select table_name 'Table', table_comment 'Description', create_time 'CreateTime' from information_schema.tables where table_schema = (select database())";

        const string MssqlViewSql = @"SELECT s.Name [Table],Convert(varchar(max),tbp.value) as Description FROM sysobjects s LEFT JOIN sys.extended_properties as tbp ON s.id=tbp.major_id and tbp.minor_id=0  AND (tbp.Name='MS_Description' OR tbp.Name is null) WHERE s.xtype IN('V') ";
        const string MssqlProcSql = @"SELECT s.Name [Table],Convert(varchar(max),tbp.value) as Description FROM sysobjects s LEFT JOIN sys.extended_properties as tbp ON s.id=tbp.major_id and tbp.minor_id=0  AND (tbp.Name='MS_Description' OR tbp.Name is null) WHERE s.xtype IN('P') ";
        #endregion

        #region 列Sql
        const string MssqlColumnSql = @"SELECT 
                d.name TableName,
                ColumnName= a.name,
                IsIdentity= case when COLUMNPROPERTY( a.id,a.name,'IsIdentity')=1 then '√'else '' end,
                DataType= b.name,
                MaxLength = COLUMNPROPERTY(a.id,a.name,'PRECISION'),
                IsNullable= case when a.isnullable=1 then '√'else '' end,
                DefaultSetting= isnull(e.text,''),
                ColumnDescription= isnull(g.[value],'')
                FROM 
                    syscolumns a
                left join 
                    systypes b 
                on 
                    a.xusertype=b.xusertype
                inner join 
                    sysobjects d 
                on 
                    a.id=d.id  and (d.xtype='U' or d.xtype='V') and  d.name<>'dtproperties'
                left join 
                    syscomments e 
                on 
                    a.cdefault=e.id
                left join 
                sys.extended_properties   g 
                on 
                    a.id=G.major_id and a.colid=g.minor_id  
                left join
                sys.extended_properties f
                on 
                    d.id=f.major_id and f.minor_id=0
                where 
                    {0}
                group by 
                     a.name,d.name,b.name,a.id,a.isnullable,e.text,g.[value],a.colorder
                order by 
                    a.id,a.colorder";

        /// <summary>
        /// MySql字段
        /// </summary>
        string MysqlColumnSql = @"SELECT a.column_Name AS ColumnName ,a.TABLE_NAME tableName,
                CASE WHEN a.extra = 'auto_increment' THEN '√' ELSE '' END  AS IsIdentity, 
                CASE Is_Nullable when 'YES' then '√' else '' end IsNullable,
	            data_type DataType,
	            Column_default DefaultSetting,
	            character_maximum_length MaxLength,
	            column_comment ColumnDescription
                FROM information_schema.COLUMNS  a
                LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS p ON a.table_schema = p.table_schema AND a.table_name = p.TABLE_NAME AND a.COLUMN_NAME = p.COLUMN_NAME AND p.constraint_name='PRIMARY'
                WHERE a.table_schema = @database and {0}
                ORDER BY a.ordinal_position  ";
        #endregion

        /// <summary>
        /// 查看对应数据库空间占用情况
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AgileDataBaseSpaceOutput>> FindDataBaseSpaceused()
        {
            const string procName = @"System_Proc_Spaceused";
            return await new SqlMapperUtil(string.Empty, connectionType, connectionString).StoredProcWithParams<AgileDataBaseSpaceOutput>(procName, null, false);
        }

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AgileDataBaseTableDto>> FindDataBaseTable()
        {
            string tableSql;
            switch (connectionType)
            {
                case ResourceDataBaseType.Mysql:
                    tableSql = MysqlTableSql;
                    break;
                default:
                    tableSql = MssqlTableSql;
                    break;
            }
            return await new SqlMapperUtil(string.Empty, connectionType, connectionString).SqlWithParams<AgileDataBaseTableDto>(tableSql, false);
        }
        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AgileDataBaseTableDto>> FindDataBaseView()
        {
            string tableSql;
            switch (connectionType)
            {
                case ResourceDataBaseType.Mysql:
                    tableSql = MysqlTableSql;
                    break;
                default:
                    tableSql = MssqlViewSql;
                    break;
            }
            return await new SqlMapperUtil(string.Empty, connectionType, connectionString).SqlWithParams<AgileDataBaseTableDto>(tableSql, false);
        }
        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AgileDataBaseTableDto>> FindDataBaseProc()
        {
            string tableSql;
            switch (connectionType)
            {
                case ResourceDataBaseType.Mysql:
                    tableSql = MysqlTableSql;
                    break;
                default:
                    tableSql = MssqlProcSql;
                    break;
            }
            return await new SqlMapperUtil(string.Empty, connectionType, connectionString).SqlWithParams<AgileDataBaseTableDto>(tableSql, false);
        }

        /// <summary>
        /// 获取对应表列信息
        /// </summary>
        /// <param name="doubleWayDto"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AgileDataBaseColumnDto>> FindDataBaseColumns(AgileDataBaseTableDto doubleWayDto)
        {
            if (doubleWayDto.DataType == "proc")
            {
                var parms = new DynamicParameters();
                string procName = doubleWayDto.Name;
                var dataTable = await new SqlMapperUtil(string.Empty, connectionType, connectionString).StoredProcWithParamsDataTable<dynamic>(procName, parms, false);
                IList<AgileDataBaseColumnDto> dataBaseColumnDoubleWays = new List<AgileDataBaseColumnDto>();
                foreach (DataColumn item in dataTable.Columns)
                {
                    dataBaseColumnDoubleWays.Add(new AgileDataBaseColumnDto
                    {
                        Name = item.ColumnName,
                        Description = ""
                    });
                }
                return dataBaseColumnDoubleWays;
            }
            else
            {
                //根据不同类型的数据库
                string columnSql;
                switch (connectionType)
                {
                    case ResourceDataBaseType.Mysql:
                        columnSql = string.Format(MysqlColumnSql, $"  a.table_name in ({doubleWayDto.Name.InSql()})"); ;
                        break;
                    default:
                        columnSql = string.Format(MssqlColumnSql, $" d.name in ({doubleWayDto.Name.InSql()})");
                        break;
                }

                return await new SqlMapperUtil(string.Empty, connectionType, connectionString).SqlWithParams<AgileDataBaseColumnDto>(columnSql,
                    new
                    {
                        tablename = doubleWayDto.Name,
                    });
            }
        }

        /// <summary>
        /// 获取外键信息
        /// </summary>
        /// <param name="doubleWayDto"></param>
        /// <returns></returns>
        public Task<IEnumerable<AgileDataBaseFkColumnOutput>> FinddatabsefFkColumn(AgileDataBaseTableDto doubleWayDto)
        {
            return new SqlMapperUtil().SqlWithParams<AgileDataBaseFkColumnOutput>(FkSql);
        }
    }
}