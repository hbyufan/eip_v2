/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/

using EIP.Agile.Models.Dtos.DataBase;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EIP.Agile.Repository
{
    /// <summary>
    /// 数据来源
    /// </summary>
    public interface IAgileDataSourceRepository
    {
        /// <summary>
        /// 查看对应数据库空间占用情况
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<AgileDataBaseSpaceOutput>> FindDataBaseSpaceused();

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<AgileDataBaseTableDto>> FindDataBaseTable();

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<AgileDataBaseTableDto>> FindDataBaseView();

        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<AgileDataBaseTableDto>> FindDataBaseProc();

        /// <summary>
        /// 获取对应表列信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<IEnumerable<AgileDataBaseColumnDto>> FindDataBaseColumns(AgileDataBaseTableDto input);

        /// <summary>
        /// 获取外键信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<IEnumerable<AgileDataBaseFkColumnOutput>> FinddatabsefFkColumn(AgileDataBaseTableDto input);

    }
}