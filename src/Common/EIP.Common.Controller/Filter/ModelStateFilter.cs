﻿/**************************************************************
* Copyright (C) 2018 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using Microsoft.AspNetCore.Mvc.Filters;

namespace EIP.Common.Controller.Filter
{
    public class ModelStateFilter : ActionFilterAttribute
    {
        /// <summary>
        /// 验证模型认证是否通过
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            //OperateStatus operateStatus = new OperateStatus();
            //if (context.ModelState.IsValid)
            //    return;
            //if (!context.ModelState.IsValid)
            //{
            //    var msg = string.Empty;
            //    foreach (var value in context.ModelState.Values)
            //    {
            //        if (value.Errors.Count > 0)
            //        {
            //            foreach (var error in value.Errors)
            //            {
            //                msg = msg + error.ErrorMessage + ",";
            //            }
            //        }
            //    }
            //    operateStatus.Message = msg.TrimEnd(',');
            //    throw new Exception(operateStatus.Message.ToString());
            //}
        }
    }
}