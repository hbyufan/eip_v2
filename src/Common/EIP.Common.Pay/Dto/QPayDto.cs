﻿using System.ComponentModel.DataAnnotations;

namespace EIP.Common.Pay.Dto
{
    public class QPayMicroPayDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }

        
        [Display(Name = "body")]
        public string Body { get; set; }

        
        [Display(Name = "fee_type")]
        public string FeeType { get; set; }

        
        [Display(Name = "total_fee")]
        public int TotalFee { get; set; }

        
        [Display(Name = "spbill_create_ip")]
        public string SpBillCreateIp { get; set; }

        
        [Display(Name = "device_info")]
        public string DeviceInfo { get; set; }

        
        [Display(Name = "auth_code")]
        public string AuthCode { get; set; }

        
        [Display(Name = "trade_type")]
        public string TradeType { get; set; }

        
        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }
    }

    public class QPayUnifiedOrderDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }

        
        [Display(Name = "body")]
        public string Body { get; set; }

        
        [Display(Name = "fee_type")]
        public string FeeType { get; set; }

        
        [Display(Name = "total_fee")]
        public int TotalFee { get; set; }

        
        [Display(Name = "spbill_create_ip")]
        public string SpBillCreateIp { get; set; }

        
        [Display(Name = "trade_type")]
        public string TradeType { get; set; }

        
        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }
    }

    public class QPayOrderQueryDto
    {
        [Display(Name = "transaction_id")]
        public string TransactionId { get; set; }

        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
    }

    public class QPayReverseDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
    }

    public class QPayCloseOrderDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
    }

    public class QPayRefundDto
    {
        
        [Display(Name = "out_refund_no")]
        public string OutRefundNo { get; set; }

        [Display(Name = "transaction_id")]
        public string TransactionId { get; set; }

        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }

        
        [Display(Name = "refund_fee")]
        public int RefundFee { get; set; }

        
        [Display(Name = "op_user_id")]
        public string OpUserId { get; set; }

        
        [Display(Name = "op_user_passwd")]
        public string OpUserPasswd { get; set; }
    }

    public class QPayRefundQueryDto
    {
        [Display(Name = "refund_id")]
        public string RefundId { get; set; }

        [Display(Name = "out_refund_no")]
        public string OutRefundNo { get; set; }

        [Display(Name = "transaction_id")]
        public string TransactionId { get; set; }

        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
    }

    public class QPayStatementDownDto
    {
        
        [Display(Name = "bill_date")]
        public string BillDate { get; set; }

        
        [Display(Name = "bill_type")]
        public string BillType { get; set; }

        [Display(Name = "tar_type")]
        public string TarType { get; set; }
    }

    public class QPayB2CPayDto
    {
        [Display(Name = "openid")]
        public string OpenId { get; set; }

        [Display(Name = "uin")]
        public string Uin { get; set; }

        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }

        [Display(Name = "total_fee")]
        
        public int TotalFee { get; set; }

        [Display(Name = "memo")]
        public string Memo { get; set; }

        [Display(Name = "check_real_name")]
        public string CheckRealName { get; set; }

        
        [Display(Name = "op_user_id")]
        public string OpUserId { get; set; }

        
        [Display(Name = "op_user_passwd")]
        public string OpUserPasswd { get; set; }

        
        [Display(Name = "spbill_create_ip")]
        public string SpBillCreateIp { get; set; }

        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }
    }
}
