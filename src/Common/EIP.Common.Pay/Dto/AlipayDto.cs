﻿using System.ComponentModel.DataAnnotations;

namespace EIP.Common.Pay.Dto
{
    public class AlipayTradePreCreateDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
        
        [Display(Name = "subject")]
        public string Subject { get; set; }

        [Display(Name = "body")]
        public string Body { get; set; }
        
        [Display(Name = "total_amount")]
        public string TotalAmount { get; set; }

        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }
    }

    public class AlipayTradePayDto
    {
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
        
        [Display(Name = "subject")]
        public string Subject { get; set; }
        
        [Display(Name = "scene")]
        public string Scene { get; set; }
        
        [Display(Name = "auth_code")]
        public string AuthCode { get; set; }

        [Display(Name = "body")]
        public string Body { get; set; }
        
        [Display(Name = "total_amount")]
        public string TotalAmount { get; set; }
    }

    public class AlipayTradeAppPayDto
    {
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
        
        [Display(Name = "subject")]
        public string Subject { get; set; }

        [Display(Name = "product_code")]
        public string ProductCode { get; set; }

        [Display(Name = "body")]
        public string Body { get; set; }
        
        [Display(Name = "total_amount")]
        public string TotalAmount { get; set; }

        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }
    }

    public class AlipayTradePagePayDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
        
        [Display(Name = "subject")]
        public string Subject { get; set; }
        
        [Display(Name = "product_code")]
        public string ProductCode { get; set; }

        [Display(Name = "body")]
        public string Body { get; set; }
        
        [Display(Name = "total_amount")]
        public string TotalAmount { get; set; }

        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }

        [Display(Name = "return_url")]
        public string ReturnUrl { get; set; }
    }

    public class AlipayTradeWapPayDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
        
        [Display(Name = "subject")]
        public string Subject { get; set; }
        
        [Display(Name = "product_code")]
        public string ProductCode { get; set; }

        [Display(Name = "body")]
        public string Body { get; set; }
        
        [Display(Name = "total_amount")]
        public string TotalAmount { get; set; }

        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }

        [Display(Name = "return_url")]
        public string ReturnUrl { get; set; }
    }

    public class AlipayTradeQueryDto
    {
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }

        [Display(Name = "trade_no")]
        public string TradeNo { get; set; }
    }

    public class AlipayTradeRefundDto
    {
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }

        [Display(Name = "trade_no")]
        public string TradeNo { get; set; }

        [Display(Name = "refund_amount")]
        public string RefundAmount { get; set; }

        [Display(Name = "out_request_no")]
        public string OutRequestNo { get; set; }

        [Display(Name = "refund_reason")]
        public string RefundReason { get; set; }
    }

    public class AlipayTradeRefundQueryDto
    {
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }

        [Display(Name = "trade_no")]
        public string TradeNo { get; set; }

        [Display(Name = "out_request_no")]
        public string OutRequestNo { get; set; }
    }
    
    public class AlipayTransferDto
    {
        
        [Display(Name = "out_biz_no")]
        public string OutBizNo { get; set; }
        
        [Display(Name = "payee_account")]
        public string PayeeAccount { get; set; }
        
        [Display(Name = "payee_type")]
        public string PayeeType { get; set; }
        
        [Display(Name = "amount")]
        public string Amount { get; set; }

        [Display(Name = "remark")]
        public string Remark { get; set; }
    }

    public class AlipayTransQueryDto
    {
        [Display(Name = "out_biz_no")]
        public string OutBizNo { get; set; }

        [Display(Name = "order_id")]
        public string OrderId { get; set; }
    }

    public class AlipayBillDownloadurlQueryDto
    {
        
        [Display(Name = "bill_type")]
        public string BillType { get; set; }
        
        [Display(Name = "bill_date")]
        public string BillDate { get; set; }
    }
}
