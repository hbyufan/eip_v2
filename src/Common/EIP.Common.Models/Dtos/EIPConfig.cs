﻿/**************************************************************
* Copyright (C) 2018 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
namespace EIP.Common.Models.Dtos
{
    /// <summary>
    /// 配置类
    /// </summary>
    public class EIPConfig
    {
        /// <summary>
        /// 密码Key
        /// </summary>
        public string PasswordKey { get; set; }

        /// <summary>
        /// 上传地址
        /// </summary>
        public string UploadPath { get; set; }

        /// <summary>
        /// 日志位置
        /// </summary>
        public string LogPath { get; set; }

        /// <summary>
        /// 缓存
        /// </summary>
        public string Cache { get; set; }

        /// <summary>
        /// 数据库连接类型
        /// </summary>
        public string ConnectionType { get; set; }
    }
}