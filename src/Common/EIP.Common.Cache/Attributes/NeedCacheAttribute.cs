﻿using System;

namespace EIP.Common.Cache.Attributes
{
    /// <summary>
    /// 缓存处理模式
    /// </summary>
    public enum CacheMode
    {
        /// <summary>
        /// 全表缓存
        /// </summary>
        AllValue = 0
    }

    /// <summary>
    /// 需要缓存时配置的特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public  class NeedCacheAttribute : Attribute
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="cacheMode">缓存处理模式</param>
        /// <param name="expiry">有效期:默认30天</param>
        public NeedCacheAttribute(CacheMode cacheMode, TimeSpan expiry)
        {
            CacheMode = cacheMode;
            Expiry = expiry;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        public NeedCacheAttribute() : this(CacheMode.AllValue)
        {
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="cacheMode">缓存处理模式</param>
        public NeedCacheAttribute(CacheMode cacheMode) : this(cacheMode, new TimeSpan(30,0,0))
        {
        }

        /// <summary>
        /// 缓存处理模式
        /// </summary>
        public CacheMode CacheMode { get; set; }

        /// <summary>
        /// 有效期:默认用完就过期
        /// </summary>
        public TimeSpan Expiry { get; set; }
    }
}
