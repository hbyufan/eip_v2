﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Exceptions;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using EIP.Common.Config;
using EIP.Common.Extension;
using EIP.Common.Message.Sms.Dto;
using System;

namespace EIP.Common.Message.Sms.Aliyun
{
    public class AliyunSms
    {
        
        /// <summary>
        /// 产品名称:云通信短信API产品,开发者无需替换
        /// </summary>
        private readonly string product = "Dysmsapi";

        /// <summary>
        /// 产品域名,开发者无需替换
        /// </summary>
        private readonly string domain = "dysmsapi.aliyuncs.com";
        private readonly SystemSmsTemplate _smsTemplate;
        public AliyunSms(SystemSmsTemplate smsTemplate)
        {
            _smsTemplate = smsTemplate;
        }
        public SendSmsResponse Send(SendSmsInput input)
        {
            var accessKeyId = _smsTemplate.AppId;
            var accessKeySecret = _smsTemplate.AppKey;
            var sign = _smsTemplate.Sign;
            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            //DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();
            SendSmsResponse response = new SendSmsResponse();
            try
            {
                //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
                request.PhoneNumbers = input.Phone;
                //必填:短信签名-可在短信控制台中找到
                request.SignName = sign;
                //必填:短信模板-可在短信控制台中找到
                request.TemplateCode = _smsTemplate.TemplateId;
                //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                request.TemplateParam = input.TemplParams.ListToJsonString();
                //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
                request.OutId = input.Ext;
                //请求失败这里会抛ClientException异常
                response = acsClient.GetAcsResponse(request);

            }
            catch (ServerException e)
            {
                response.Code = e.ErrorCode;
                response.Message = e.ErrorMessage;
            }
            catch (ClientException e)
            {
                response.Code = e.ErrorCode;
                response.Message = e.ErrorMessage;
            }
            return response;
        }
    }
}
