import { SystemGroupAll, SystemOrganizationTopOrg } from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 组织架构
 */
export function organization(topOrg) {
    return request(SystemOrganizationTopOrg + "/" + topOrg, METHOD.GET, {})
}

/**
 * 角色
 */
export function group() {
    return request(SystemGroupAll, METHOD.GET, {})
}

export default {
    organization,
    group
}