import { AgileConfigQuery, AgileConfigDelete, SystemDictionaryFindByParentIds, } from '@/services/api'
import { request, METHOD } from '@/utils/request'
/**
 * 树
 */
export function dictionaryQueryByIds(param) {
    return request(SystemDictionaryFindByParentIds, METHOD.POST, param)
}
/**
 * 列表
 */
export function query(param) {
    return request(AgileConfigQuery, METHOD.POST, param)
}

/**
 * 删除
 */
export async function del(param) {
    return request(AgileConfigDelete, METHOD.POST, param)
}

export default {
    dictionaryQueryByIds,
    query,
    del
}